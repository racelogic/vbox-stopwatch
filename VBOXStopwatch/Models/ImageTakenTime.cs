﻿using System;
using SQLite;
namespace VBOXStopwatch.Models
{
    [Table("ImageTakenTime")]
    public class ImageTakenTime
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string carName { get; set; }
        public int listNumber { get; set; }
        public string imageTakeTime { get; set; }
    }
}
