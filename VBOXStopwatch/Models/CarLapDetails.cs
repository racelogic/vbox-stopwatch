﻿using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;

namespace VBOXStopwatch.Models
{
    public class CarLapDetails
    {
        public byte[] CarImage { get; set; }
        public string CarName { get; set; }
        public List<string>Laps { get; set; }
        public List<int> LapNumber { get; set; }
        public int BestLapNumber { get; set; }

        public CarLapDetails()
        {
        }
    }
}
