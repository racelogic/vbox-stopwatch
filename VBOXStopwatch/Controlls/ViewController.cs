﻿using Foundation;
using System;
using UIKit;
using AVFoundation;
using System.Threading.Tasks;
using Photos;
using System.Collections.Generic;
using CoreGraphics;
//using Xamarians.CropImage;
//using RSKImageCropper;
using Xamarin.TOCropView;
//using SQLite;
using System.IO; //Vish Current version
using VBOXStopwatch.Models;
using MobileCoreServices;
using System.Linq;
using System.Timers;

namespace VBOXStopwatch
{
    public partial class ViewController : UIViewController, IUIScrollViewDelegate, IUIDragInteractionDelegate, IUIDropInteractionDelegate, IUITableViewDropDelegate//, IRSKImageCropViewControllerDelegate
    {
        UICollectionView carsCollView;

        //CropViewDataSource cropDelegate;
        public List<UIImage> fetchedImages = new List<UIImage>();
        public List<string> imagesTakenTimes = new List<string>();

        //For Scrollview 
        int carImgIndex = 0;
        UIImageView dragImage;

        List<CarsInfo> carsData = new List<CarsInfo>();
        List<ImageTakenTime> imagestime = new List<ImageTakenTime>();

        CropImage i = new CropImage();

        List<DateTime> startTimes = new List<DateTime>();

        Dictionary<string, List<string>> carsInfoData = new Dictionary<string, List<string>>();
        Dictionary<string, TimeSpan> carsBestLapInfo = new Dictionary<string, TimeSpan>();
        Dictionary<string, List<TimeSpan>> carsLapData = new Dictionary<string, List<TimeSpan>>();

        Dictionary<string, List<string>> lastAddedCarInfoData = new Dictionary<string, List<string>>();

        Dictionary<string, float> lapDuePercentages = new Dictionary<string, float>();
        Dictionary<string, DateTime> lapStartTime = new Dictionary<string, DateTime>();

        int selectedCarsCount = 1;
        int destinationTableRow = 0;

        string existingCarName = "";
        UIImage existingImage = new UIImage();

        List<int> selectedImgsForDeleting = new List<int>();

        List<UIColor> colr = new List<UIColor>();
        List<string> colorsAry = new List<string>();

        UIScrollView sclView = new UIScrollView();

        public DateTime exitTime;
        public DateTime startTime;

        private string pathToDatabase;
        public bool isNewCar = false;
        public bool isNewSession = false;

        UIImageView placeholderImage;

        UIView rightView;
        UIView leftView;
        UIImageView rightImage;
        UIImageView leftImage;

        UILabel carsCountLabel;


        //For Capturing Image
        AVCaptureSession session;
        AVCaptureDeviceInput inputDevice;
        AVCaptureStillImageOutput stillImageOutput;
        AVCaptureVideoPreviewLayer videoPreviewLayer;

        int carsCount = 1;

        int bestLapsCar = 0;//21st sep

        private UIImpactFeedbackGenerator _impactHeavyFeedbackGenerator;
        private Timer timer1 = new Timer();
        float x = 0f;

        //Used for Undo action, and also finding which car has been updated recently
        int activeRow = 0;
        List<int> activites = new List<int>();
        public List<UIImage> backUpImages = new List<UIImage>();
        public List<string> backUpImagesTakenTimes = new List<string>();


        //For Timer lable in the cell
        Timer clock;
        int min = 0, seconds = 0, milliseconds = 0;

        int xPositionValue = 0;

        public bool showPreview = true;

        //int vish = 0;
        public void InitTimer()
        {
            //timer1 = new Timer();
            timer1.Elapsed += (sender, eventArgs) =>
            {
                //Console.WriteLine("Timing...: "+vish);
                //calculateDueAlertPercentages();
                checkForTimerInvalidate();
                calculateDueAlertPercentages();
                //vish += 1;
                //tickTickTimer();
                //globalTickView();
                //x += 0.01f;
            };
            timer1.Interval = 1000;
            timer1.Start();
        }

        public void HapticFeedbackHelperIOS()
        {
            _impactHeavyFeedbackGenerator = new UIImpactFeedbackGenerator(UIImpactFeedbackStyle.Heavy);
        }

        public ViewController(IntPtr handle) : base(handle)
        {

        }
        /*
            public ViewController(DateTime t)
            {
                exitTime = t;
            }
        */
        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();
            await AuthorizeCameraUse();
            //InitTimer();
            //Console.WriteLine(i.test());
            undoRedoButton.Enabled = false;
            UIApplication.SharedApplication.IdleTimerDisabled = true;

            bool ToggleSwitch = NSUserDefaults.StandardUserDefaults.BoolForKey("ToggleSwitch");

            livePreviewView.Hidden = false; //!ToggleSwitch; //true;

            //var statusBarView = UIApplication.SharedApplication.ValueForKey(new NSString("statusBar")) as UIView;
            //statusBarView.BackgroundColor = UIColor.Green;

            //Fetching photos from photos apps and storing it in "fetchedImages" array
            //fetchImagesFromPhotosApp();

            dragImage = new UIImageView();

            //dragImage.Image = UIImage.FromBundle("car1");

            topView.Layer.MaskedCorners = (CoreAnimation.CACornerMask)3;
            topView.Layer.CornerRadius = 15;

            //if (fetchedImages.Count > 0)
            //{
            //    //Adding images to the scrollview with image darg functionality
            //    //setUpScrlView(fetchedImages.Count);
            //    scrlView(fetchedImages.Count);
            //    dragImage.Image = fetchedImages[0];
            //}

            setupDropTableView();

            NavigationController.NavigationBarHidden = true;
            lapButtonClicked.Layer.CornerRadius = 4;
            lapButtonClicked.Layer.BorderColor = UIColor.White.CGColor;
            lapButtonClicked.Layer.BorderWidth = 2;

            colr.Add(UIColor.Yellow);
            colr.Add(UIColor.Orange);
            colr.Add(UIColor.Gray);
            colr.Add(UIColor.Blue);
            colr.Add(UIColor.LightGray);
            colr.Add(UIColor.Green);
            colr.Add(UIColor.Magenta);
            colr.Add(UIColor.Red);

            colorsAry.Add("Yellow");
            colorsAry.Add("Orange");
            colorsAry.Add("Gray");
            colorsAry.Add("Blue");
            colorsAry.Add("LightGray");
            colorsAry.Add("Green");
            colorsAry.Add("Magenta");
            colorsAry.Add("Red");

            NSNotificationCenter.DefaultCenter.AddObserver(new NSString("ShowPreview"), ShowCamPreview);
            NSNotificationCenter.DefaultCenter.AddObserver(new NSString("SelectedImages"), deleteSelectedImages);
            NSNotificationCenter.DefaultCenter.AddObserver(new NSString("ShowLapsList"), ShowLapsListOfACar);
            NSNotificationCenter.DefaultCenter.AddObserver(new NSString("ResetData"), reseDatatNotification);
            NSNotificationCenter.DefaultCenter.AddObserver(new NSString("ClearSession"), resetSessionData);
            NSNotificationCenter.DefaultCenter.AddObserver(new NSString("DeleteCarsInfoDataBase"), deleteCarsInfoDateBase);

            NSNotificationCenter.DefaultCenter.AddObserver(new NSString("ImagesCount"), updateImagesCountLabel);
            NSNotificationCenter.DefaultCenter.AddObserver(new NSString("ChangeName"), getExistingData);
            NSNotificationCenter.DefaultCenter.AddObserver(new NSString("ChangeImage"), getExistingImageData);
            NSNotificationCenter.DefaultCenter.AddObserver(new NSString("CroppedImage"), getCroppedImage);
            string exitTimeStr1 = NSUserDefaults.StandardUserDefaults.StringForKey("ExitTime");
            NSUserDefaults.StandardUserDefaults.RemoveObject("ExitTime");
            string exitTimeStr = NSUserDefaults.StandardUserDefaults.StringForKey("ExitTime");
            /*
            var prefs = NSUserDefaults.StandardUserDefaults;
            NSDictionary dic = prefs.ToDictionary();
            foreach (object key in dic.Keys)
            {
                prefs.RemoveObject(key.ToString());
            }
            */

            createDatabase();
            fetchDataFromLocalStorage();

            //For Capturing Image
            SetupLiveCameraStream();
            //showHideView.Hidden = true;
            refreshButton.Enabled = false;

            // Allow dropping.
            //https://github.com/xamarin/ios-samples/blob/main/ios11/DragAndDropCustomView/CustomViewDragAndDrop/ViewController.cs#L131
            var dropInteraction = new UIDropInteraction(this);
            bottomView.AddInteraction(dropInteraction);

            //startTime = DateTime.Now;
            CarsListView.AllowsSelection = true;

            //rightView.Hidden = true;
            //leftView.Hidden = true;
            //rightImage.Hidden = true;
            //leftImage.Hidden = true;

            //vish
            addScrollView();

            UITapGestureRecognizer gesture = new UITapGestureRecognizer(bigLapButtonClicked);
            gesture.NumberOfTapsRequired = 1;
            showHideView.AddGestureRecognizer(gesture);

            if(carsInfoData.Count == 0)
            {
                CarsListView.BackgroundView = new UIImageView(UIImage.FromBundle("imageDrop"));
            }
            else
            {
                CarsListView.BackgroundView = new UIImageView(UIImage.FromBundle(""));
            }
           
            isNewSession = false;

            //showCamButton.SetImage(UIImage.FromBundle("CameraHide"), forState: UIControlState.Normal);
            //showCamButton.TintColor = UIColor.White;
        }

        void createDatabase()
        {
            var documentsFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            pathToDatabase = Path.Combine(documentsFolder, "carsInfo_db.db");

            using (var connection = new SQLite.SQLiteConnection(pathToDatabase))
            {
                connection.CreateTable<CarsInfo>();
                connection.CreateTable<ImageTakenTime>();
            }
        }

        void createCarsInfoTable()
        {
            var documentsFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            pathToDatabase = Path.Combine(documentsFolder, "carsInfo_db.db");

            using (var connection = new SQLite.SQLiteConnection(pathToDatabase))
            {
                connection.CreateTable<CarsInfo>();
            }
        }

        void dropCarsInfoTable()
        {
            using (var connection = new SQLite.SQLiteConnection(pathToDatabase))
            {
                connection.DropTable<CarsInfo>(); //CreateTable<CarsInfo>();
            }
        }

        void dropDatabaseTable()
        {
            //var documentsFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            //pathToDatabase = Path.Combine(documentsFolder, "carsInfo_db.db");

            using (var connection = new SQLite.SQLiteConnection(pathToDatabase))
            {
                connection.DropTable<CarsInfo>(); //CreateTable<CarsInfo>();
                connection.DropTable<ImageTakenTime>();
            }
        }

        public void fetchDataFromLocalStorage()
        {
            using (var connection = new SQLite.SQLiteConnection(pathToDatabase))
            {
                var query = connection.Table<CarsInfo>();
                foreach (CarsInfo carsDetails in query)
                {
                    //Console.WriteLine("Row Number: " + carsDetails.rowNumber);
                    carsData.Add(carsDetails);
                    //carsInfoData[info.CarName] = ary;
                }

                var imagestimeQuery = connection.Table<ImageTakenTime>();

                foreach (ImageTakenTime imagesTime in imagestimeQuery)
                {
                    imagestime.Add(imagesTime);
                }
                var imgsClockTime = new Dictionary<string, List<string>>();
                List<string> car1 = new List<string>();
                List<string> car2 = new List<string>();
                List<string> car3 = new List<string>();
                List<string> car4 = new List<string>();
                List<string> car5 = new List<string>();
                List<string> car6 = new List<string>();
                List<string> car7 = new List<string>();
                List<string> car8 = new List<string>();
                List<string> car9 = new List<string>();
                List<string> car10 = new List<string>();

                //for(int i=0; i<imagestime.Count; i++)
                //{
                //    List<string> timesAry = new List<string>();
                //    if (imgsClockTime.ContainsKey(imagestime[i].carName))
                //    {
                //        timesAry = carsInfoData[imagestime[i].carName];
                //        timesAry.Add(imagestime[i].imageTakeTime);
                //        carsInfoData[imagestime[i].carName] = timesAry;
                //    } else
                //    {
                //        timesAry.Add(imagestime[i].imageTakeTime);
                //        carsInfoData[imagestime[i].carName] = timesAry; //timesAry.Add(imagestime[i].imageTakeTime);
                //    }
                //}


                for (int i = 0; i < imagestime.Count; i++)
                {
                    //imgsClockTime[imagestime[i].carName] = imagestime[i].imageTakeTime;
                    if (imagestime[i].listNumber == 0)
                    {
                        car1.Add(imagestime[i].imageTakeTime);
                        carsInfoData[imagestime[i].carName] = car1;
                    }
                    else if (imagestime[i].listNumber == 1)
                    {
                        car2.Add(imagestime[i].imageTakeTime);
                        carsInfoData[imagestime[i].carName] = car2;
                    }
                    else if (imagestime[i].listNumber == 2)
                    {
                        car3.Add(imagestime[i].imageTakeTime);
                        carsInfoData[imagestime[i].carName] = car3;
                    }
                    else if (imagestime[i].listNumber == 3)
                    {
                        car4.Add(imagestime[i].imageTakeTime);
                        carsInfoData[imagestime[i].carName] = car4;
                    }
                    else if (imagestime[i].listNumber == 4)
                    {
                        car5.Add(imagestime[i].imageTakeTime);
                        carsInfoData[imagestime[i].carName] = car5;
                    }
                    else if (imagestime[i].listNumber == 5)
                    {
                        car6.Add(imagestime[i].imageTakeTime);
                        carsInfoData[imagestime[i].carName] = car6;
                    }
                    else if (imagestime[i].listNumber == 6)
                    {
                        car7.Add(imagestime[i].imageTakeTime);
                        carsInfoData[imagestime[i].carName] = car7;
                    }
                    else if (imagestime[i].listNumber == 7)
                    {
                        car8.Add(imagestime[i].imageTakeTime);
                        carsInfoData[imagestime[i].carName] = car8;
                    }
                    else if (imagestime[i].listNumber == 8)
                    {
                        car9.Add(imagestime[i].imageTakeTime);
                        carsInfoData[imagestime[i].carName] = car8;
                    }
                    else if (imagestime[i].listNumber == 9)
                    {
                        car10.Add(imagestime[i].imageTakeTime);
                        carsInfoData[imagestime[i].carName] = car8;
                    }
                }
                calculateAllTheLaps(carsInfoData);
                //Console.WriteLine(imagestime.Count);
                CarsListView.ReloadData();
            }
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            NavigationController.NavigationBarHidden = true;
            //Console.WriteLine("Lap button exit time: " + exitTime);
            string exitTimeStr = NSUserDefaults.StandardUserDefaults.StringForKey("ExitTime");
            //Console.WriteLine("user defaults value: " + exitTimeStr);
            /*
            var isLaunchedformQuitState = false;
            using (var connection = new SQLite.SQLiteConnection(pathToDatabase))
            {
                var query = connection.Table<CarsInfo>();
                foreach (CarsInfo carsDetails in query)
                {
                    Console.WriteLine("Row Number: " + carsDetails.rowNumber);
                    //for(int i=0; i < carsData.Count; i++)
                    //{
                    //    if (carsData[i].rowNumber ==)
                    //}
                    carsData.Add(carsDetails);
                    isLaunchedformQuitState = true;
                }
            }
            string isRelaunched = NSUserDefaults.StandardUserDefaults.StringForKey("AppState");
            if (isLaunchedformQuitState)
            {
                CarsListView.ReloadData();
            }
            else
            {
            */
            if (exitTimeStr != null)
            {
                exitTime = Convert.ToDateTime(exitTimeStr);

                //var startDateTime = startTime.ToString("dd-mm-yyyy HH:mm:ss");
                //var exitDateTime = exitTime.ToString("dd-mm-yyyy HH:mm:ss");

                var startDateTime = string.Format("{0}-{1}-{2} {3}:{4}:{5}.{6}", startTime.Day, startTime.Month, startTime.Year, startTime.Hour, startTime.Minute, startTime.Second, startTime.Millisecond);
                var exitDateTime = string.Format("{0}-{1}-{2} {3}:{4}:{5}.{6}", exitTime.Day, exitTime.Month, exitTime.Year, exitTime.Hour, exitTime.Minute, exitTime.Second, exitTime.Millisecond);

                //fetchPhotosInRange(DateTime.Parse("14-10-2020 11:00:00"), DateTime.Parse("17-10-2020 18:38:00"));//dd-mm-yyyy HH:mm:ss
                //Console.WriteLine("Exit time: " + exitTime);
                //Console.WriteLine("Formatted Exit time: " + exitDateTime);
                //Console.WriteLine("-------------------------------------");
                //Console.WriteLine("Start Time: " + startTime);
                //Console.WriteLine("Formatted start time: " + startDateTime);

                //Console.WriteLine(imagesTakenTimes.Count);
                //Console.WriteLine(fetchedImages.Count);


                //int value = DateTime.Compare(exitTime, startTime);
                var duration = exitTime - startTime;
                if (duration.Minutes > 0 || duration.Seconds > 0)
                {
                    fetchedImages.Clear();
                    imagesTakenTimes.Clear();
                    fetchPhotosInRange(DateTime.Parse(startDateTime), DateTime.Parse(exitDateTime));
                    sclView.RemoveFromSuperview();
                    NSUserDefaults.StandardUserDefaults.StringForKey("ExitTime");
                    scrlView(fetchedImages.Count);
                }
            }
            //}
        }

        void carsCollectionView()
        {
            var layout = new UICollectionViewLayout();
            carsCollView.Frame = new CGRect(8, 13, UIScreen.MainScreen.Bounds.Width - 140, topView.Frame.Height - 17);
        }

        void photoSwipeGuides()
        {
            rightView = new UIView(new CGRect(sclView.Frame.Width - 12, sclView.Frame.Y + 12 + 8, 20, sclView.Frame.Height));
            rightView.BackgroundColor = UIColor.Clear;

            rightView.Hidden = true;
            rightImage = new UIImageView(new CGRect(0, rightView.Frame.Height / 2 - 10, 20, 20));
            rightImage.Image = UIImage.FromBundle("right");

            rightView.AddSubview(rightImage);
            View.AddSubview(rightView);
            View.BringSubviewToFront(rightView);

            leftView = new UIView(new CGRect(sclView.Frame.X, sclView.Frame.Y + 12 + 8, 20, sclView.Frame.Height));
            leftView.BackgroundColor = UIColor.Clear;

            leftView.Hidden = true;
            leftImage = new UIImageView(new CGRect(0, leftView.Frame.Height / 2 - 10, 20, 20));
            leftImage.Image = UIImage.FromBundle("left");

            leftView.AddSubview(leftImage);
            View.AddSubview(leftView);
            View.BringSubviewToFront(leftView);

            carsCountLabel = new UILabel(new CGRect(sclView.Frame.Width - 23, sclView.Frame.Height, 30, 30));
            //carsCountLabel = new UILabel(new CGRect(sclView.Frame.Width - 23, CarsListView.Frame.Y, 30, 30));
            carsCountLabel.Text = "0";
            carsCountLabel.Font = UIFont.SystemFontOfSize(24);
            //carsCountLabel.BackgroundColor = UIColor.Red;
            carsCountLabel.TextColor = UIColor.FromRGB(255, 0, 255);
            carsCountLabel.TextAlignment = UITextAlignment.Center;
            carsCountLabel.Hidden = true;
            View.AddSubview(carsCountLabel);
            //sclView.AddSubview(carsCountLabel);
            View.BringSubviewToFront(carsCountLabel);
            //sclView.BringSubviewToFront(carsCountLabel);
        }

        void addScrollView()
        {
            sclView.Frame = new CGRect(16, 13, UIScreen.MainScreen.Bounds.Width - 160, topView.Frame.Height - 17);//31March
            sclView.PagingEnabled = true;
            sclView.DecelerationRate = UIScrollView.DecelerationRateFast;

            //plus 20 is the height of the status bar
            placeholderImage = new UIImageView(new CGRect(8, sclView.Frame.Y + 20, sclView.Frame.Width, sclView.Frame.Height));
            placeholderImage.Image = UIImage.FromBundle("VBOXLogo");
            placeholderImage.ContentMode = UIViewContentMode.ScaleAspectFit;
            placeholderImage.Hidden = false;
            View.AddSubview(placeholderImage);
            View.BringSubviewToFront(placeholderImage);

            topView.AddSubview(sclView);
            if (rightView == null && leftView == null)
            {
                photoSwipeGuides();
            }

            sclView.Scrolled += delegate
            {
                var pageWidth = sclView.Frame.Width;
                var page = (int)Math.Floor((sclView.ContentOffset.X - pageWidth / 2) / pageWidth) + 1;
                //pageControl.CurrentPage = page;
                carImgIndex = page;
                //dragImage = new UIImageVie();
                NSData data = fetchedImages[page].AsJPEG((nfloat)0.05);
                //var imageData = NSData.FromStream(stream);
                var image = UIImage.LoadFromData(data);

                dragImage.Image = MaxResizeImage(fetchedImages[page], 20, 20);//viiiiii//image;//fetchedImages[page]; //UIImage.FromBundle(imageNames[0]);//fetchedImages[page];
                //Console.WriteLine(carImgIndex);

                //foreach (UIView v in sclView.Subviews)
                //{
                //    //if (v as UIImageView)
                //    //{
                //    //}
                //    if(v is UIScrollViewIndicatorStyle)
                //    {
                //        v.BackgroundColor = UIColor.Red;
                //    }
                //    Console.WriteLine(v);
                //}

                if ((xPositionValue / sclView.Frame.Width) >= 1)
                {
                    if (fetchedImages.Count == 1)
                    {
                        rightView.Hidden = true;
                        leftView.Hidden = true;
                    }
                    else if (carImgIndex == ((xPositionValue / sclView.Frame.Width) - 1))
                    {
                        rightView.Hidden = true;
                        leftView.Hidden = false;
                    }
                    else if (carImgIndex == 0)
                    {
                        rightView.Hidden = false;
                        leftView.Hidden = true;
                    }
                    else
                    {
                        rightView.Hidden = false;
                        leftView.Hidden = false;
                    }
                }
                else
                {
                    rightView.Hidden = true;
                    leftView.Hidden = true;
                }
            };
        }

        void scrlView(int count)
        {

            if (count <= 0)
            {
                return;
            }
            sclView.Frame = new CGRect(8, 13, UIScreen.MainScreen.Bounds.Width - 140, topView.Frame.Height - 17);
            sclView.PagingEnabled = true;
            sclView.DecelerationRate = UIScrollView.DecelerationRateFast;

            topView.AddSubview(sclView);
            if (rightView == null && leftView == null)
            {
                photoSwipeGuides();
            }

            var imageNames = new List<String>() { "car1", "car2", "car3", "car4", "car5", "car6", "car7", "car8" };

            sclView.Scrolled += delegate
            {
                var pageWidth = sclView.Frame.Width;
                var page = (int)Math.Floor((sclView.ContentOffset.X - pageWidth / 2) / pageWidth) + 1;
                //pageControl.CurrentPage = page;
                carImgIndex = page;
                //dragImage = new UIImageVie();
                NSData data = fetchedImages[page].AsJPEG((nfloat)0.05);
                //var imageData = NSData.FromStream(stream);
                var image = UIImage.LoadFromData(data);

                dragImage.Image = MaxResizeImage(fetchedImages[page], 20, 20);//viiiiii//image;//fetchedImages[page]; //UIImage.FromBundle(imageNames[0]);//fetchedImages[page];
                //Console.WriteLine(carImgIndex);

                if (count > 1)
                {
                    if (carImgIndex == (count - 1))
                    {
                        rightView.Hidden = true;
                        leftView.Hidden = false;
                    }
                    else if (carImgIndex == 0)
                    {
                        rightView.Hidden = false;
                        leftView.Hidden = true;
                    }
                    else
                    {
                        rightView.Hidden = false;
                        leftView.Hidden = false;
                    }
                }
            };

            for (int i = 0; i < count; i++)
            {
                var carImgView = new UIImageView(new CGRect(i * sclView.Frame.Width, 0, sclView.Frame.Width, sclView.Frame.Height));
                carImgView.Image = fetchedImages[i];
                carImgView.Tag = i;
                sclView.AddSubview(carImgView);

                //UITapGestureRecognizer gesture = new UITapGestureRecognizer(Tap);
                //gesture.NumberOfTapsRequired = 1;
                //carImgView.AddGestureRecognizer(gesture);

                var carTimeLbl = new UILabel(new CGRect(0, sclView.Frame.Height - 30, sclView.Frame.Width, 30));
                carTimeLbl.Text = imagesTakenTimes[i];
                carTimeLbl.TextColor = UIColor.White;
                carTimeLbl.TextAlignment = UITextAlignment.Right;
                carImgView.AddSubview(carTimeLbl);

                var dragAction = new UIDragInteraction(this);
                dragAction.Enabled = true;
                carImgView.AddInteraction(dragAction);
                carImgView.UserInteractionEnabled = true;
            }
            sclView.ContentSize = new CGSize(sclView.Frame.Width * count, sclView.Frame.Height);
            nfloat w = sclView.Frame.Width * count;
            //sclView.ContentOffset.X = sclView.Frame.Width * count;

            CGRect frame = sclView.Frame;
            frame.X = frame.Width * (count - 1);
            frame.Y = 0;
            sclView.ScrollRectToVisible(frame, false);
        }

        void Tap(UITapGestureRecognizer tap1)
        {
            var selctedView = tap1.View;
            Console.WriteLine(selctedView.Tag);
            int tagValue = (int)selctedView.Tag;
            if (selectedImgsForDeleting.Contains(tagValue))
            {
                selectedImgsForDeleting.Remove(tagValue);
                selctedView.Layer.BorderColor = UIColor.Clear.CGColor;
                selctedView.Layer.BorderWidth = .0f;
            }
            else
            {
                selectedImgsForDeleting.Add(tagValue);
                selctedView.Layer.BorderColor = UIColor.Red.CGColor;
                selctedView.Layer.BorderWidth = 2.0f;
            }
            NSNotificationCenter.DefaultCenter.PostNotificationName("SelectedImages", null);
        }

        void bigLapButtonClicked(UITapGestureRecognizer taped)
        {
            if (showPreview)
            {
                captureImage();
            }
            else
            {
                var imageCaptureAlert = UIAlertController.Create("Error", "You cant capture image with camera preview set to OFF, please turn it ON", UIAlertControllerStyle.Alert);
                imageCaptureAlert.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel, alert => Console.WriteLine("Cancel was clicked")));
                PresentViewController(imageCaptureAlert, true, null);
            }
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        void setupDropTableView()
        {
            CarsListView.Source = new CarsListViewDataSource(carsData, carsLapData);
            CarsListView.DropDelegate = this;
        }

        void updateImagesCountLabel(NSNotification notification)
        {
            carsCountLabel.Text = string.Format("{0}", fetchedImages.Count);
            var count = carsCountLabel.Text;
            var intcount = int.Parse(count);
            if (intcount == 0)
            {
                carsCountLabel.Hidden = true;
                placeholderImage.Hidden = false;
            }
            else
            {
                carsCountLabel.Hidden = false;
                placeholderImage.Hidden = true;
            }
        }

        void reseDatatNotification(NSNotification notification)
        {
            deleteAllTables();
        }

        void deleteCarsInfoDateBase(NSNotification notification)
        {
            dropCarsInfoTable();
            createCarsInfoTable();
        }

        void resetSessionData(NSNotification notification)
        {
            timer1.Stop();
            var value = notification.UserInfo["newSession"];
            if (value.ToString() == "1")
            {
                isNewSession = true;
            }
            /*
            activites.Clear();
            selectedCarsCount = 1;
            carsBestLapInfo.Clear();
            undoRedoButton.Enabled = false;

            //For clearing the due alert timings
            lapStartTime.Clear();
            //topView.RemoveFromSuperview(sclView);
            fetchedImages.Clear();
            imagesTakenTimes.Clear();

            backUpImages.Clear();
            backUpImagesTakenTimes.Clear();

            //sclView.RemoveFromSuperview();

            foreach (UIView subview in sclView.Subviews)
            {
                subview.RemoveFromSuperview();
            }

            CarsListView.ReloadData();

            rightView.Hidden = true;
            leftView.Hidden = true;
            refreshButton.Enabled = false;

            xPositionValue = 0;
            carsCountLabel.Text = "0";
            CarsListView.BackgroundView = new UIImageView(UIImage.FromBundle("imageDrop"));
            */

            // Delete all the data, but before doing that capture car images and its names. Then insert into tableview 
            Console.WriteLine(carsData);
            dropDatabaseTable();
            createDatabase();
            foreach (CarsInfo data in carsData)
            {
                //Console.WriteLine(data.)
                data.BestLapTime = "Best - ";
                data.RecentLapTime = "LAP 1  Waiting..";
                data.Lap1 = "";
                data.Lap2 = "";
                data.Lap3 = "";
                data.Lap4 = "";
                data.duePercent = 0;
                data.isNew = true;
            }

            Dictionary<string, List<string>> previousData = new Dictionary<string, List<string>>();

            foreach (KeyValuePair<string, List<string>> data in carsInfoData)
            {
                previousData[data.Key] = new List<string>();
            }
            carsInfoData = previousData;

            foreach (CarsInfo data in carsData)
            {
                using (var connection = new SQLite.SQLiteConnection(pathToDatabase))
                {
                    connection.Insert(data);
                }
            }

            activites.Clear();
            selectedCarsCount = 1;
            carsBestLapInfo.Clear(); // Remove all the values
            undoRedoButton.Enabled = false;

            //For clearing the due alert timings
            lapStartTime.Clear(); //Remove all the values and set the values to empty
            //topView.RemoveFromSuperview(sclView);
            fetchedImages.Clear();
            imagesTakenTimes.Clear();

            backUpImages.Clear();
            backUpImagesTakenTimes.Clear();

            //sclView.RemoveFromSuperview();

            foreach (UIView subview in sclView.Subviews)
            {
                subview.RemoveFromSuperview();
            }

            rightView.Hidden = true;
            leftView.Hidden = true;
            refreshButton.Enabled = false;

            xPositionValue = 0;
            carsCountLabel.Text = "0";
            carsCountLabel.Hidden = true;
            CarsListView.ReloadData();
        }

        void deleteAllTables()
        {
            carsCount = 1;

            isNewSession = false;
            dropDatabaseTable();
            carsData.Clear();
            activites.Clear();

            carsInfoData.Clear();
            carsBestLapInfo.Clear();
            selectedCarsCount = 1;
            //timer1.Stop();

            undoRedoButton.Enabled = false;
            //For clearing the due alert timings
            lapStartTime.Clear();
            //topView.RemoveFromSuperview(sclView);
            fetchedImages.Clear();
            imagesTakenTimes.Clear();

            backUpImages.Clear();
            backUpImagesTakenTimes.Clear();

            //sclView.RemoveFromSuperview();

            foreach (UIView subview in sclView.Subviews)
            {
                subview.RemoveFromSuperview();
            }

            CarsListView.ReloadData();

            rightView.Hidden = true;
            leftView.Hidden = true;
            refreshButton.Enabled = false;

            xPositionValue = 0;
            carsCountLabel.Text = "0";
            carsCountLabel.Hidden = true;
            CarsListView.BackgroundView = new UIImageView(UIImage.FromBundle("imageDrop"));
        }


        // Checking drop action
        #region IUIDropInteractionDelegate

        [Export("dropInteraction:canHandleSession:")]
        public bool CanHandleSession(UIDropInteraction interaction, IUIDropSession session)
        {
            return session.HasConformingItems(new[] { UTType.Image.ToString() }) && session.Items.Length == 1;
        }

        [Export("dropInteraction:sessionDidEnter:")]
        public void SessionDidEnter(UIDropInteraction interaction, IUIDropSession session)
        {
            var dropLocation = session.LocationInView(bottomView);
            UpdateLayers(dropLocation);
        }

        /// <summary>
        /// Specify how to handle the dropped items.
        /// </summary>
        [Export("dropInteraction:sessionDidUpdate:")]
        public UIDropProposal SessionDidUpdate(UIDropInteraction interaction, IUIDropSession session)
        {
            var dropLocation = session.LocationInView(bottomView);
            UpdateLayers(dropLocation);

            var operation = new UIDropOperation();

            if (dragImage.Frame.Contains(dropLocation))
            {
                operation = session.LocalDragSession == null ? UIDropOperation.Copy : UIDropOperation.Move;
            }
            else
            {
                // Cancel dropping if it's not inside the image view.
                operation = UIDropOperation.Cancel;
            }

            return new UIDropProposal(operation);
        }


        /// <summary>
        /// Access the drag item object when drop is performed.
        /// </summary>
        //[Export("dropInteraction:performDrop:")]
        public void PerformDrop(UIDropInteraction interaction, IUIDropSession session)
        {
            // Get the drag items (UIImage in this case).
            session.LoadObjects<UIImage>(images => {
                dragImage.Image = images?.First();
            });

            var dropLocation = session.LocationInView(bottomView);
            UpdateLayers(dropLocation);
        }

        /*   */
        [Export("dropInteraction:sessionDidExit:")]
        public void SessionDidExit(UIDropInteraction interaction, IUIDropSession session)
        {
            var dropLocation = session.LocationInView(bottomView);
            UpdateLayers(dropLocation);
        }

        [Export("dropInteraction:sessionDidEnd:")]
        public void SessionDidEnd(UIDropInteraction interaction, IUIDropSession session)
        {
            var dropLocation = session.LocationInView(bottomView);
            //UpdateLayers(dropLocation);
            UpdateScrollviewsByRemovingImages();
        }


        void UpdateLayers(CGPoint dropLocation)
        {
            //I have added different colors and corner radius just to idnetify when to perform delete action and when to not
            if (View.Frame.Contains(dropLocation))
            {
                bottomView.BackgroundColor = UIColor.Orange;
            }
            else
            {
                bottomView.BackgroundColor = UIColor.FromRGB(0, 53, 101);
            }
        }

        void UpdateScrollviewsByRemovingImages()
        {
            /*
             * For removing the image from the above list
            backUpImages.Add(fetchedImages[carImgIndex]);
            backUpImagesTakenTimes.Add(imagesTakenTimes[carImgIndex]);

            fetchedImages.RemoveAt(carImgIndex);
            imagesTakenTimes.RemoveAt(carImgIndex);
            sclView.RemoveFromSuperview();
            scrlView(fetchedImages.Count);
            bottomView.Layer.CornerRadius = 0;

            if (fetchedImages.Count != 0)
            {
                dragImage.Image = MaxResizeImage(fetchedImages[carImgIndex], 100, 100); //fetchedImages[carImgIndex];
            }
            */

            // Consume drag items
            List<UIImage> stringItems = new List<UIImage>();
            NSIndexPath indexPath;

            // Get last index path of table view
            var section = CarsListView.NumberOfSections() - 1;
            var row = CarsListView.NumberOfRowsInSection(section);
            indexPath = NSIndexPath.FromRowSection(row, section);

            updateTableViewRows(indexPath);
            var indexPaths = new List<NSIndexPath>();
            indexPaths.Add(indexPath);
            CarsListView.InsertRows(indexPaths.ToArray(), UITableViewRowAnimation.Automatic);

            if (isNewCar)
            {
                CarsListView.ScrollToRow(indexPath, UITableViewScrollPosition.Bottom, true);
            }
            bottomView.BackgroundColor = UIColor.FromRGB(0, 53, 101);
        }

        #endregion
        // Checking drop action


        /// <summary>
        /// Access the drag item object when drop is performed.
        /// </summary>
        public void PerformDrop(UITableView tableView, IUITableViewDropCoordinator coordinator)
        {
            NSIndexPath indexPath, destinationIndexPath;

            // TODO: confirm this port is accurate
            if (coordinator.DestinationIndexPath != null)
            {
                indexPath = coordinator.DestinationIndexPath;
                destinationIndexPath = indexPath;
            }
            else
            {
                // Get last index path of table view
                var section = tableView.NumberOfSections() - 1;
                var row = tableView.NumberOfRowsInSection(section);
                destinationIndexPath = NSIndexPath.FromRowSection(row, section);
            }
            destinationTableRow = destinationIndexPath.Row;
            Console.WriteLine("Destination row is "+destinationTableRow);

            coordinator.Session.LoadObjects<UIImage>((items) =>
            {
                // Consume drag items
                List<UIImage> stringItems = new List<UIImage>();
                updateTableViewRows(destinationIndexPath);
                var indexPaths = new List<NSIndexPath>();
                indexPaths.Add(destinationIndexPath);
                tableView.InsertRows(indexPaths.ToArray(), UITableViewRowAnimation.Automatic);
                if (isNewCar)
                {
                    CarsListView.ScrollToRow(destinationIndexPath, UITableViewScrollPosition.Bottom, true);
                }
            });
        }

        string convertSecondsIntoMMSSFFFFormat(double seconds)
        {
            TimeSpan t = TimeSpan.FromSeconds(seconds);

            string answer = string.Format("{0:D2}:{1:D2}:{2:D3}",
                            t.Minutes,
                            t.Seconds,
                            t.Milliseconds);
            return answer;
        }

        string tickTickTimer()
        {
            /*
                //Cell
                //NSIndexPath path = NSIndexPath.FromRowSection(0, 0);
                //var cell = (CarsCustomCell)CarsListView.CellAt(path);
                //cell.recentLap.Text = "KOTA";
            */
            /*
            TimeSpan t = TimeSpan.FromSeconds(840);

            string answer = string.Format("{0:D2}:{1:D2}:{2:D3}",
                            t.Minutes,
                            t.Seconds,
                            t.Milliseconds);
            min = t.Minutes;
            seconds = t.Seconds;
            milliseconds = t.Milliseconds;
            */

            var str = "";
            milliseconds++;
            if (milliseconds >= 1000)
            {
                seconds++;
                milliseconds = 0;
            }
            if (seconds == 59)
            {
                min++;
                seconds = 0;
            }
            str = string.Format("{0:00}:{1:00}:{2:000}", min, seconds, milliseconds);
            return str;
        }

        void updateTableViewRows(NSIndexPath dest)
        {
            var rowCount = CarsListView.NumberOfRowsInSection(0);

            var droppedImg = new UIImage();
            var droppedName = "";

            if (carsData.Count == 0)
            {
                createDatabase();
            }

            droppedImg = fetchedImages[carImgIndex];
            droppedName = imagesTakenTimes[carImgIndex];
            selectedImgsForDeleting.Remove(carImgIndex);
            CarsListView.SeparatorStyle = UITableViewCellSeparatorStyle.None;

            //var imagesTimeList = new ImageTakenTime();
            //imagesTimeList.rowNumber = dest.Row;
            //imagesTimeList.imageTakeTime = droppedName;
            //imagesTimeList.carName = "bbCar " + selectedCarsCount;
            //using (var connection = new SQLite.SQLiteConnection(pathToDatabase))
            //{ 
            //    connection.Insert(imagesTimeList); //CreateTable<CarsInfo>();
            //}

            // I need to sort out what all data needs to be shown to end user ()

            var info = new CarsInfo();
            /*
            clock = new Timer();
            clock.Interval = 1;
            clock.Elapsed += (sender, eventArgs) =>
            {
                string countdown = tickTickTimer();
                info.RecentLapTime = "";
            };
            clock.Start();
            */
            //Vish
            TimeSpan diff = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff")) - Convert.ToDateTime(droppedName);
            //info.RecentLapTime = convertSecondsIntoMMSSFFFFormat(diff.TotalSeconds);

            
            info.RecentLapTime = "LAP 1  Waiting..";//"Recent   --:--";//droppedName.Split(' ')[1];
            //info.CarImage = droppedImg;
            using (NSData imageData = droppedImg.AsJPEG())
            {
                Byte[] myByteArray = new Byte[imageData.Length];
                info.CarImage = imageData.ToArray();//myByteArray;

                //System.Runtime.InteropServices.Marshal.Copy(imageData.Bytes, myByteArray, 0, Convert.ToInt32(imageData.Length));
            }
            
            info.rowNumber = dest.Row;


            //info.Name = "Car "+
            //;
            activeRow = dest.Row;
            activites.Add(dest.Row);

            var imagesTimeList = new ImageTakenTime();
            imagesTimeList.listNumber = dest.Row;

            /*
                lapsInfo.Add(info.CarName);
                carsInfoData[info.Name] = lapsInfo;
                Console.WriteLine("Information: " + carsInfoData.Keys + " Values: "+ carsInfoData.Values);
            */
            //storeCarsInfo(info.Name, info.CarName);

            if (dest.Row >= rowCount)
            {
                /* 
                 * Whatever we are adding to info i.e., CarsInfo.cs model 
                 * will be stored in the local database
                 * */
                isNewCar = true;

                info.CarName = "Car " + carsCount;//"Car " + (carsData.Count + 1);//selectedCarsCount;
                info.BestLapTime = "Best - ";
                //info.CarColor = colr[selectedCarsCount - 1];
                info.Lap1 = "";
                info.Lap2 = "";
                info.Lap3 = "";
                info.Lap4 = "";
                info.lapColor = colorsAry[0];
                info.isNew = false;

                info.timerValue = "";

                info.duePercent = 0; 
                List<string> ary = new List<string>();
                ary.Add(droppedName);
                carsInfoData[info.CarName] = ary;
                //info.imageTakeTime = ary;
                lastAddedCarInfoData[info.CarName] = ary;
                AppDelegate.carsInfoDataDelegate[info.CarName] = ary;
                carsData.Insert(dest.Row, info);
                
                using (var connection = new SQLite.SQLiteConnection(pathToDatabase))
                { 
                    connection.Insert(info); //CreateTable<CarsInfo>();
                }

                //var imagesTimeList = new ImageTakenTime();
                //imagesTimeList.listNumber = dest.Row;
                imagesTimeList.imageTakeTime = droppedName;
                imagesTimeList.carName = info.CarName;
                using (var connection = new SQLite.SQLiteConnection(pathToDatabase))
                {
                    connection.Insert(imagesTimeList); //CreateTable<CarsInfo>();
                }
                carsCount += 1;
            }
            else if (isNewSession && carsData[dest.Row].isNew)
            {
                var previousData = carsData[dest.Row];

                isNewCar = true;
                info.CarName = previousData.CarName;//selectedCarsCount;
                info.BestLapTime = "Best - ";
                //info.CarColor = colr[selectedCarsCount - 1];
                info.Lap1 = "";
                info.Lap2 = "";
                info.Lap3 = "";
                info.Lap4 = "";
                info.lapColor = colorsAry[0];
                info.RecentLapTime = "LAP 1  Waiting..";
                info.duePercent = 0;
                info.CarImage = previousData.CarImage;

                info.isNew = false;

                carsData.RemoveAt(dest.Row);
                deleteRecord(dest.Row);

                CarsListView.ReloadData();

                List<string> ary = new List<string>();
                ary.Add(droppedName);
                carsInfoData[info.CarName] = ary;
                //info.imageTakeTime = ary;
                lastAddedCarInfoData[info.CarName] = ary;
                AppDelegate.carsInfoDataDelegate[info.CarName] = ary;
                carsData.Insert(dest.Row, info);

                using (var connection = new SQLite.SQLiteConnection(pathToDatabase))
                {
                    connection.Insert(info); //CreateTable<CarsInfo>();
                }

                //var imagesTimeList = new ImageTakenTime();
                //imagesTimeList.listNumber = dest.Row;
                imagesTimeList.imageTakeTime = droppedName;
                imagesTimeList.carName = info.CarName;
                using (var connection = new SQLite.SQLiteConnection(pathToDatabase))
                {
                    connection.Insert(imagesTimeList); //CreateTable<CarsInfo>();
                }
            }
            else
            {
                isNewCar = false;
                InitTimer();
                var value = carsData[dest.Row];
                var existingImg = value.CarImage;

                //carsInfo[dest.Row].CarName = droppedName;

                //List<string> arys = new List<string>();
                //arys.Add(value.CarName);
                //arys.Add(droppedName);

                List<string> arys = new List<string>();
                arys = carsInfoData[value.CarName];
                arys.Add(droppedName);

                //var imagesTimeList = new ImageTakenTime();
                //imagesTimeList.listNumber = dest.Row;
                imagesTimeList.imageTakeTime = droppedName;
                imagesTimeList.carName = value.CarName;
                using (var connection = new SQLite.SQLiteConnection(pathToDatabase))
                {
                    connection.Insert(imagesTimeList); //CreateTable<CarsInfo>();
                }

                carsInfoData[value.CarName] = arys;
                AppDelegate.carsInfoDataDelegate[value.CarName] = arys;

                calculateAllTheLaps(carsInfoData);
                //Console.WriteLine("Cars Best lap infomation" + carsBestLapInfo);
                //Console.WriteLine("Cars full laps information" + carsLapData);

                var lapNumber = carsLapData[value.CarName].Count;
                //Console.WriteLine("Cars last index value: " + carsLapData[value.CarName][lapNumber - 1]);
                var currentLapInfo = carsLapData[value.CarName][lapNumber - 1];
                var lapMillSec = currentLapInfo.Milliseconds.ToString("000").Substring(0, 2);
                var formatedStr = string.Format("{0:D2}:{1:D2}.{2}", currentLapInfo.Minutes, currentLapInfo.Seconds, lapMillSec);
                //var formatedStr = string.Format("{0:D2}:{1:D1}", currentLapInfo.Minutes, currentLapInfo.Seconds);

                TimeSpan lap1Info = new TimeSpan();
                TimeSpan lap2Info = new TimeSpan();
                TimeSpan lap3Info = new TimeSpan();
                TimeSpan lap4Info = new TimeSpan();


                if (lapNumber > 3)
                {
                    lap1Info = carsLapData[value.CarName][lapNumber - 4];
                    var lap1MilliSec = lap1Info.Milliseconds.ToString("000").Substring(0, 2);
                    info.Lap1 = string.Format("{3}: {0:D2}:{1:D2}.{2}", lap1Info.Minutes, lap1Info.Seconds, lap1MilliSec, (lapNumber - 3));
                    //info.Lap1 = string.Format("{2}: {0:D2}:{1:D1}", lap1Info.Minutes, lap1Info.Seconds, (lapNumber - 3));

                    TimeSpan lap1 = calculateWithReferenceLap(carsLapData, value.CarName, lapNumber - 4);
                        var lap1DiffMilliSec = lap1.Milliseconds.ToString("000").Substring(0, 2);
                        var lap1Str = string.Format("{0:D1}.{1:D2}", lap1.Seconds, lap1.Milliseconds);
                        if (float.Parse(lap1Str) == 0.00)
                        {
                            lap1Str = "";
                        }
                        else if (float.Parse(lap1Str) > 0.00) // if (float.Parse(lap1Str) >= 0.00)
                        {
                            lap1Str = string.Format("+{0:D1}.{1}", lap1.Seconds, lap1DiffMilliSec);
                        }
                        else 
                        {
                            lap1Str = string.Format("-{0:D1}.{1}", lap1.Seconds, lap1DiffMilliSec);
                        }
                        info.Lap1 = string.Format("{0} {1}", info.Lap1, lap1Str);

                    lap2Info = carsLapData[value.CarName][lapNumber - 3];
                    var lap2MilliSec = lap2Info.Milliseconds.ToString("000").Substring(0, 2);
                    info.Lap2 = string.Format("{3}: {0:D2}:{1:D2}.{2}", lap2Info.Minutes, lap2Info.Seconds, lap2MilliSec, (lapNumber - 2));
                    //info.Lap2 = string.Format("{2}: {0:D2}:{1:D1}", lap2Info.Minutes, lap2Info.Seconds, (lapNumber - 2));

                    TimeSpan lap2 = calculateWithReferenceLap(carsLapData, value.CarName, lapNumber - 3);
                        var lap2DiffMilliSec = lap2.Milliseconds.ToString("000").Substring(0, 2);
                        var lap2Str = string.Format("{0:D1}.{1:D2}", lap2.Seconds, lap2.Milliseconds);
                        if (float.Parse(lap2Str) == 0.00)
                        {
                            lap2Str = "";
                        }
                        else if (float.Parse(lap2Str) > 0.00) // if (float.Parse(lap2Str) >= 0.00)
                        {
                            lap2Str = string.Format("+{0:D1}.{1}", lap2.Seconds, lap2DiffMilliSec);
                        }
                        else
                        {
                            lap2Str = string.Format("-{0:D1}.{1}", lap2.Seconds, lap2DiffMilliSec);
                        }
                        info.Lap2 = string.Format("{0} {1}", info.Lap2, lap2Str);

                    lap3Info = carsLapData[value.CarName][lapNumber - 2];
                    var lap3MilliSec = lap3Info.Milliseconds.ToString("000").Substring(0, 2);
                    info.Lap3 = string.Format("{3}: {0:D2}:{1:D2}.{2}", lap3Info.Minutes, lap3Info.Seconds, lap3MilliSec, (lapNumber - 1));
                    //info.Lap3 = string.Format("{2}: {0:D2}:{1:D1}", lap3Info.Minutes, lap3Info.Seconds, (lapNumber - 1));

                    TimeSpan lap3 = calculateWithReferenceLap(carsLapData, value.CarName, lapNumber - 2);
                        var lap3DiffMilliSec = lap3.Milliseconds.ToString("000").Substring(0, 2);
                        var lap3Str = string.Format("{0:D1}.{1:D2}", lap3.Seconds, lap3.Milliseconds);
                        if (float.Parse(lap3Str) == 0.00)
                        {
                            lap3Str = "";
                        }
                        else if (float.Parse(lap3Str) > 0.00) // if (float.Parse(lap3Str) >= 0.00)
                        {
                            lap3Str = string.Format("+{0:D1}.{1}", lap3.Seconds, lap3DiffMilliSec);
                        }
                        else
                        {
                            lap3Str = string.Format("-{0:D1}.{1}", lap3.Seconds, lap3DiffMilliSec);
                        }
                        info.Lap3 = string.Format("{0} {1}", info.Lap3, lap3Str);

                    lap4Info = carsLapData[value.CarName][lapNumber - 1];
                    var lap4MilliSec = lap4Info.Milliseconds.ToString("000").Substring(0, 2);
                    info.Lap4 = string.Format("{3}: {0:D2}:{1:D2}.{2}", lap4Info.Minutes, lap4Info.Seconds, lap4MilliSec, (lapNumber - 0));
                    //info.Lap4 = string.Format("{2}: {0:D2}:{1:D1}", lap4Info.Minutes, lap4Info.Seconds, (lapNumber - 0));

                    TimeSpan lap4 = calculateWithReferenceLap(carsLapData, value.CarName, lapNumber - 1);
                        var lap4DiffMilliSec = lap4.Milliseconds.ToString("000").Substring(0, 2);
                        var lap4Str = string.Format("{0:D1}.{1:D2}", lap4.Seconds, lap4.Milliseconds);
                        if (float.Parse(lap4Str) == 0.00)
                        {
                            lap4Str = "";
                        }
                        else if (float.Parse(lap4Str) > 0.00) // if (float.Parse(lap4Str) >= 0.00)
                        {
                            lap4Str = string.Format("+{0:D1}.{1}", lap4.Seconds, lap4MilliSec);
                        }
                        else
                        {
                            lap4Str = string.Format("-{0:D1}.{1}", lap4.Seconds, lap4MilliSec);
                        }
                        info.Lap4 = string.Format("{0} {1}", info.Lap4, lap4Str);
                }
                else if (lapNumber > 2)
                {
                    lap1Info = carsLapData[value.CarName][lapNumber - 3];
                    var lap1MilliSec = lap1Info.Milliseconds.ToString("000").Substring(0, 2);
                    info.Lap1 = string.Format("{3}: {0:D2}:{1:D2}.{2}", lap1Info.Minutes, lap1Info.Seconds, lap1MilliSec, (lapNumber - 2));
                    //info.Lap1 = string.Format("{2}: {0:D2}:{1:D1}", lap1Info.Minutes, lap1Info.Seconds, (lapNumber - 2));

                    TimeSpan lap1 = calculateWithReferenceLap(carsLapData, value.CarName, lapNumber - 3);
                        var lap1DiffMilliSec = lap1.Milliseconds.ToString("000").Substring(0, 2);
                        var lap1Str = string.Format("{0:D1}.{1:D2}", lap1.Seconds, lap1.Milliseconds);
                        if (float.Parse(lap1Str) == 0.00)
                        {
                            lap1Str = "";
                        }
                        else if (float.Parse(lap1Str) > 0.00) // if (float.Parse(lap1Str) >= 0.00)
                        {
                            lap1Str = string.Format("+{0:D1}.{1}", lap1.Seconds, lap1DiffMilliSec);
                        }
                        else
                        {
                            lap1Str = string.Format("-{0:D1}.{1}", lap1.Seconds, lap1DiffMilliSec);
                        }
                        info.Lap1 = string.Format("{0} {1}", info.Lap1, lap1Str);

                    lap2Info = carsLapData[value.CarName][lapNumber - 2];
                    var lap2MilliSec = lap2Info.Milliseconds.ToString("000").Substring(0, 2);
                    info.Lap2 = string.Format("{3}: {0:D2}:{1:D2}.{2}", lap2Info.Minutes, lap2Info.Seconds, lap2MilliSec, (lapNumber - 1));
                    //info.Lap2 = string.Format("{2}: {0:D2}:{1:D1}", lap2Info.Minutes, lap2Info.Seconds, (lapNumber - 1));

                    TimeSpan lap2 = calculateWithReferenceLap(carsLapData, value.CarName, lapNumber - 2);
                        var lap2DiffMilliSec = lap2.Milliseconds.ToString("000").Substring(0, 2);
                        var lap2Str = string.Format("{0:D1}.{1:D2}", lap2.Seconds, lap2.Milliseconds);
                        if (float.Parse(lap2Str) == 0.00)
                        {
                            lap2Str = "";
                        }
                        else if (float.Parse(lap2Str) > 0.00) // if (float.Parse(lap2Str) >= 0.00)
                        {
                            lap2Str = string.Format("+{0:D1}.{1}", lap2.Seconds, lap2DiffMilliSec);
                        }
                        else
                        {
                            lap2Str = string.Format("-{0:D1}.{1}", lap2.Seconds, lap2DiffMilliSec);
                        }
                        info.Lap2 = string.Format("{0} {1}", info.Lap2, lap2Str);

                    lap3Info = carsLapData[value.CarName][lapNumber - 1];
                    var lap3MilliSec = lap3Info.Milliseconds.ToString("000").Substring(0, 2);
                    info.Lap3 = string.Format("{3}: {0:D2}:{1:D2}.{2}", lap3Info.Minutes, lap3Info.Seconds, lap3MilliSec, (lapNumber - 0));
                    //info.Lap3 = string.Format("{2}: {0:D2}:{1:D1}", lap3Info.Minutes, lap3Info.Seconds, (lapNumber - 0));

                    TimeSpan lap3 = calculateWithReferenceLap(carsLapData, value.CarName, lapNumber - 1);
                        var lap3DiffMilliSec = lap3.Milliseconds.ToString("000").Substring(0, 2);
                        var lap3Str = string.Format("{0:D1}.{1:D2}", lap3.Seconds, lap3.Milliseconds);
                        if (float.Parse(lap3Str) == 0.00)
                        {
                            lap3Str = "";
                        }
                        else if (float.Parse(lap3Str) > 0.00) // if (float.Parse(lap3Str) >= 0.00)
                        {
                            lap3Str = string.Format("+{0:D1}.{1}", lap3.Seconds, lap3DiffMilliSec);
                        }
                        else
                        {
                            lap3Str = string.Format("-{0:D1}.{1}", lap3.Seconds, lap3DiffMilliSec);
                        }
                        info.Lap3 = string.Format("{0} {1}", info.Lap3, lap3Str);
                    info.Lap4 = "";
                }
                else if (lapNumber > 1)
                {
                    lap1Info = carsLapData[value.CarName][lapNumber - 2];
                    var lap1MilliSec = lap1Info.Milliseconds.ToString("000").Substring(0, 2);
                    info.Lap1 = string.Format("{3}: {0:D2}:{1:D2}.{2}", lap1Info.Minutes, lap1Info.Seconds, lap1MilliSec, (lapNumber - 1));
                    //info.Lap1 = string.Format("{3}: {0:D2}:{1:D2}", lap1Info.Minutes, lap1Info.Seconds, lap1MilliSec, (lapNumber - 1));

                    TimeSpan lap1 = calculateWithReferenceLap(carsLapData, value.CarName, lapNumber - 2);
                        var lap1DiffMilliSec = lap1.Milliseconds.ToString("000").Substring(0, 2);
                        var lap1Str = string.Format("{0:D1}.{1:D2}", lap1.Seconds, lap1.Milliseconds);
                        if (float.Parse(lap1Str) == 0.00)
                        {
                            lap1Str = "";
                        }
                        else if (float.Parse(lap1Str) > 0.00) // if (float.Parse(lap1Str) >= 0.00)
                        {
                            lap1Str = string.Format("+{0:D1}.{1}", lap1.Seconds, lap1DiffMilliSec);
                        }
                        else  
                        {
                            lap1Str = string.Format("-{0:D1}.{1}", lap1.Seconds, lap1DiffMilliSec);
                        }
                        info.Lap1 = string.Format("{0} {1}", info.Lap1, lap1Str);


                    lap2Info = carsLapData[value.CarName][lapNumber - 1];
                    var lap2MilliSec = lap2Info.Milliseconds.ToString("000").Substring(0, 2);
                    info.Lap2 = string.Format("{3}: {0:D2}:{1:D2}.{2}", lap2Info.Minutes, lap2Info.Seconds, lap2MilliSec, (lapNumber - 0));
                    //info.Lap2 = string.Format("{2}: {0:D2}:{1:D1}", lap2Info.Minutes, lap2Info.Seconds, (lapNumber - 0));

                    TimeSpan lap2 = calculateWithReferenceLap(carsLapData, value.CarName, lapNumber - 1);
                        var lap2DiffMilliSec = lap2.Milliseconds.ToString("000").Substring(0, 2);
                        var lap2Str = string.Format("{0:D1}.{1:D2}", lap2.Seconds, lap2.Milliseconds);
                        if (float.Parse(lap2Str) == 0.00)
                        {
                            lap2Str = "";
                        }
                        else if (float.Parse(lap2Str) > 0.00) // if (float.Parse(lap2Str) >= 0.00)
                        {
                            lap2Str = string.Format("+{0:D1}.{1}", lap2.Seconds, lap2DiffMilliSec);
                        }
                        else
                        {
                            lap2Str = string.Format("-{0:D1}.{1}", lap2.Seconds, lap2DiffMilliSec);
                        }
                        info.Lap2 = string.Format("{0} {1}", info.Lap2, lap2Str);
                    info.Lap3 = "";
                    info.Lap4 = "";
                }
                else if (lapNumber == 1)
                {
                    lap1Info = carsLapData[value.CarName][lapNumber - 1];
                    var lap1MilliSec = lap1Info.Milliseconds.ToString("000").Substring(0, 2);
                    info.Lap1 = string.Format("{3}: {0:D2}:{1:D2}.{2}", lap1Info.Minutes, lap1Info.Seconds, lap1MilliSec, (lapNumber - 0));
                    //info.Lap1 = string.Format("{3}: {0:D2}:{1:D2}", lap1Info.Minutes, lap1Info.Seconds, lap1MilliSec, (lapNumber - 0));
/*
                    TimeSpan lap1 = calculateWithReferenceLap(carsLapData, value.CarName, lapNumber - 1);
                        var lap1DiffMilliSec = lap1.Milliseconds.ToString("000").Substring(0, 2);
                        var lap1Str = string.Format("{0:D1}.{1:D2}", lap1.Seconds, lap1.Milliseconds);
                        if (float.Parse(lap1Str) >= 0.00)
                        {
                            lap1Str = string.Format("+{0:D1}.{1}", lap1.Seconds, lap1DiffMilliSec);
                        }
                        else
                        {
                            lap1Str = string.Format("-{0:D1}.{1}", lap1.Seconds, lap1DiffMilliSec);
                        }
                        info.Lap1 = string.Format("{0} {1}", info.Lap1, lap1Str);
*/
                    info.Lap2 = "";
                    info.Lap3 = "";
                    info.Lap4 = "";
                }
                info.duePercent = 0;

                info.isNew = false;

                carsData.RemoveAt(dest.Row);
                //Here I need to update the existing record
                deleteRecord(dest.Row);

                CarsListView.ReloadData();
                var appendedValue = value.CarName + "#" + droppedName; 
                info.CarImage = existingImg;

                info.RecentLapTime = string.Format("LAP {0}   {1}", lapNumber, formatedStr);
                //info.RecentLapTime = string.Format("LAP {0}  Waiting..", lapNumber+1);
                info.CarName = value.CarName;
                TimeSpan bestTime = carsBestLapInfo[value.CarName];

                //info.CarColor = value.CarColor;
                info.lapColor = value.lapColor;
                var bestLapMillSec = bestTime.Milliseconds.ToString("000").Substring(0, 2);
                info.BestLapTime = string.Format("Best - {0:D2}:{1:D2}.{2}", bestTime.Minutes, bestTime.Seconds, bestLapMillSec);//{2:D1}

                info.timerValue = "";

                carsData.Insert(dest.Row, info);
                using (var connection = new SQLite.SQLiteConnection(pathToDatabase))
                {
                    connection.Insert(info);
                }
                //calculateDueAlertPercentages();
            }
            lapStartTime[info.CarName] = DateTime.Now;
            //selectedCarsCount = carsData.Count + 1;
            backUpImages.Add(fetchedImages[carImgIndex]);
            backUpImagesTakenTimes.Add(imagesTakenTimes[carImgIndex]);

            fetchedImages.RemoveAt(carImgIndex);
            imagesTakenTimes.RemoveAt(carImgIndex);

            undoRedoButton.Enabled = true;

            //if (fetchedImages.Count != 0 && fetchedImages[carImgIndex] != null)
            //{
            //    dragImage.Image = MaxResizeImage(fetchedImages[carImgIndex], 100, 100); //fetchedImages[carImgIndex];
            //}

            /*
            // With some valid UIView *view:
            foreach (UIView subview in sclView.Subviews)
            {
                subview.RemoveFromSuperview(); 
            }
            sclView.RemoveFromSuperview();
            scrlView(fetchedImages.Count);
            */

            xPositionValue -= (int)sclView.Frame.Width;
            var check = 0;
            foreach (UIView v in sclView.Subviews)
            {
                if (v.Tag == carImgIndex && check == 0)
                {
                    v.RemoveFromSuperview();
                    check = 1;
                    continue;
                }
                if (check == 1)
                {
                    v.Frame = new CGRect(v.Frame.X - sclView.Frame.Width, 0, sclView.Frame.Width, sclView.Frame.Height);
                    v.Tag = v.Tag - 1;
                }
            }
            check = 0;
            sclView.ContentSize = new CGSize(sclView.Frame.Width * fetchedImages.Count, sclView.Frame.Height);

            if (fetchedImages.Count == 0)
            {
                refreshButton.Enabled = false;
                rightView.Hidden = true;
                leftView.Hidden = true;
            }
            
            //carsCountLabel.Text = string.Format("{0}", fetchedImages.Count);
            NSNotificationCenter.DefaultCenter.PostNotificationName("ImagesCount", null);
            CarsListView.BackgroundView = new UIImageView(UIImage.FromBundle(""));
        }

        public CarsInfo GetSingleRecord(int destRow)
        {
            using (var connection = new SQLite.SQLiteConnection(pathToDatabase))
            {
                return connection.Table<CarsInfo>().FirstOrDefault(i => i.rowNumber == destRow);
            }
            //return connection.Table<Student>().FirstOrDefault(i => i.StudentName == Name);
        }

        public void deleteRecord(int destRow)
        {
            using (var connection = new SQLite.SQLiteConnection(pathToDatabase))
            {
                connection.Delete<CarsInfo>(destRow);
            }
        }
/*
        public void UpdateRecord(CarsInfo obj)
        {
            using (var connection = new SQLite.SQLiteConnection(pathToDatabase))
            {
                return connection.Table<CarsInfo>().FirstOrDefault(i => i.rowNumber == destRow);
            }
        }
*/
        void calculateAllTheLaps(Dictionary<string, List<string>> carsInfoData)
        {
            //Dictionary<string, List<TimeSpan>> carsLapData = new Dictionary<string, List<TimeSpan>>();
            foreach (KeyValuePair<string, List<string>> data in carsInfoData)
            {
                List<string> times = data.Value;
                //Console.WriteLine("Cars Lap count: " + times.Count);

                List<TimeSpan> timesAry = new List<TimeSpan>();
                TimeSpan bestTime = new TimeSpan();
                for (int i = 0; i < times.Count; i++)
                { 
                    if (i + 1 < times.Count)
                    {
                        TimeSpan diff = Convert.ToDateTime(times[i + 1]) - Convert.ToDateTime(times[i]);
                        timesAry.Add(diff);
                        if (diff < bestTime)
                        {
                            bestTime = diff;
                        }
                        //Console.WriteLine("Current Diff: " + diff + " best time: " + bestTime);
                        if (i == 0)
                        {
                            bestTime = diff;
                        }
                        //Console.WriteLine($"{bestTime.Minutes.ToString()}:{bestTime.Seconds.ToString("00")}:{bestTime.Milliseconds.ToString("000").Substring(0, 2)}");
                    }
                }
                carsLapData[data.Key] = timesAry;
                //TimeSpan diff = Convert.ToDateTime(times.FindLast) - Convert.ToDateTime(times[0]);
                //Console.WriteLine("Time Difference: "+diff);
                carsBestLapInfo[data.Key] = bestTime;
                AppDelegate.carsBestLapDataDelegate[data.Key] = bestTime;
            }
        }

        TimeSpan calculateWithReferenceLap(Dictionary<string, List<TimeSpan>> carsLapData, string carName, int index)
        {
            List<TimeSpan> lapTimes = carsLapData[carName];
            TimeSpan referenceTime = carsBestLapInfo[carName];

            TimeSpan diff = lapTimes[index] - referenceTime;
            //Console.WriteLine(diff);

            return diff;
        }

        void globalTickView()
        {
            //Console.WriteLine("Tick Count: " + carsData.Count);

            //foreach (CarsInfo d in carsData)
            //{
            //    Console.WriteLine(d.rowNumber);
            //    d.duePercent = x;
            //}
            //new System.Threading.Thread(new System.Threading.ThreadStart(() => {
            //    InvokeOnMainThread(() =>
            //    {
            //        CarsListView.ReloadData();
            //    });
            //})).Start();
            new System.Threading.Thread(new System.Threading.ThreadStart(() =>
            {
                InvokeOnMainThread(() =>
                {
                    if (carsData.Count > 0)
                    {
                        NSIndexPath path = NSIndexPath.FromRowSection(0, 0);
                        var cell = (CarsCustomCell)CarsListView.CellAt(path);
                        cell.recentLap.Text = tickTickTimer(); //"KOTA";
                        Console.WriteLine(cell.recentLap.Text);
                        //cell.pgrView.SetProgress(data.duePercent, true);
                    }
                    //CarsListView.ReloadData();
                });
            })).Start();
        }

        void calculatetimer()
        {
            foreach(CarsInfo d in carsData)
            {
                d.RecentLapTime = "";
            }
            new System.Threading.Thread(new System.Threading.ThreadStart(() => {
                InvokeOnMainThread(() =>
                {
                    CarsListView.ReloadData();
                });
            })).Start();
        }

        void oneCarAtaTime()
        {
            //for (int i = 0; i < carsBestLapInfo.Count; i++)
            //{
            //}
            var carName = carsData[bestLapsCar].CarName;
            TimeSpan bestime = carsBestLapInfo[carName];
            double bestimeInSeconds = bestime.TotalSeconds;

            if (bestimeInSeconds == 0)
            {
                return;
            }

            DateTime startAt = lapStartTime[carName];

            DateTime currentTime = DateTime.Now;

            TimeSpan diff = currentTime - startAt;

            double dueTimePercent = 0;
            if (diff.TotalSeconds >= 0)//&& diff.TotalSeconds <= bestimeInSeconds
            {
                dueTimePercent = (diff.TotalSeconds * 100) / bestimeInSeconds;

                carsData[bestLapsCar].duePercent = Convert.ToSingle(dueTimePercent / 100);

                new System.Threading.Thread(new System.Threading.ThreadStart(() =>
                {
                    InvokeOnMainThread(() =>
                    {
                        NSIndexPath[] path = new NSIndexPath[1];// NSIndexPath.FromRowSection(0, 0);
                        path[0] = NSIndexPath.FromRowSection(bestLapsCar, 0);

                        CarsListView.ReloadRows(path, UITableViewRowAnimation.None);
                    });
                })).Start();
             
            }
        }

        //For every 2 seconds we need to call this method and reload the tableview
        void calculateDueAlertPercentages()
        {
            foreach (KeyValuePair<string, TimeSpan> data in carsBestLapInfo)
            {
                //Console.WriteLine("-----------------------");
                //Console.WriteLine("Best lap data of a car "+data.Key + " " + data.Value);

                //List<string> carsFullLapData = carsInfoData[data.Key];
                //DateTime recentLapTime = Convert.ToDateTime(data.Value);

                //lapDuePercentages[data.Key] = data.Value;

                TimeSpan bestime = data.Value;
                double bestimeInSeconds = bestime.TotalSeconds;

                if (bestimeInSeconds == 0)
                {
                    continue;
                }
                //Console.WriteLine("Best time In seconds: " + bestimeInSeconds);

                DateTime startAt = lapStartTime[data.Key];
                //Console.WriteLine("Actual lap started time: "+ startAt.ToString("yyyy-MM-dd HH:mm:ss.ffff"));

                DateTime currentTime = DateTime.Now;
                //Console.WriteLine("Current time: " + currentTime.ToString("yyyy-MM-dd HH:mm:ss.ffff"));

                TimeSpan diff = currentTime - startAt;
                //Console.WriteLine("Difference between current time and start time: "+diff.TotalSeconds);
                //Console.WriteLine("Progresss bar fill percentage of a car: "+(diff.TotalSeconds * 100) / bestimeInSeconds);
                //Console.WriteLine("Update table view " + Convert.ToSingle(((diff.TotalSeconds * 100) / bestimeInSeconds) / 100));
                //Console.WriteLine("-----------------------");

                double dueTimePercent = 0;
                if (diff.TotalSeconds >= 0)//&& diff.TotalSeconds <= bestimeInSeconds
                {
                    //Console.WriteLine("************");
                    dueTimePercent = (diff.TotalSeconds * 100) / bestimeInSeconds;
                    //Console.WriteLine(dueTimePercent / 100);
                    foreach (CarsInfo d in carsData)
                    {
                        //Console.WriteLine("Inside the CarsData loop: " + d.rowNumber);
                        if (d.CarName.Equals(data.Key))
                        {
                            d.duePercent = Convert.ToSingle(dueTimePercent / 100);
                            Console.WriteLine("Row number: " + d.rowNumber + " Due Percent: " + d.duePercent);
                            Console.WriteLine("************");

                            TimeSpan time = TimeSpan.FromSeconds(diff.TotalSeconds);
                            //here backslash is must to tell that colon is
                            //not the part of format, it just a character that we want in output
                            string minHourFormat = time.ToString(@"mm\:ss\.fff");//@"hh\:mm\:ss" or @"hh\:mm\:ss\.fff"

                            double countdownVaue = bestimeInSeconds - diff.TotalSeconds;

                            d.timerValue = Convert.ToInt16(countdownVaue).ToString();
                            Console.WriteLine("Total Seconds value: "+d.timerValue);
                            if (diff.TotalSeconds >= bestimeInSeconds)
                            {
                                break;
                            }
                            new System.Threading.Thread(new System.Threading.ThreadStart(() =>
                            {
                                InvokeOnMainThread(() =>
                                {
                                    /*
                                    NSIndexPath path = NSIndexPath.FromRowSection(d.rowNumber, 0);
                                    var cell = (CarsCustomCell)CarsListView.CellAt(path);
                                    //cell.recentLap.Text = tickTickTimer(); //"KOTA";
                                    //Console.WriteLine(cell.recentLap.Text);
                                        
                                    if (d.duePercent >= 0.85)
                                    {
                                        cell.pgrView.ProgressTintColor = UIColor.FromRGB(253, 134, 39);//UIColor.Orange;
                                    }
                                    else
                                    {
                                        cell.pgrView.ProgressTintColor = UIColor.FromRGB(137, 191, 255);//UIColor.blue;
                                    }
                                    cell.pgrView.SetProgress(d.duePercent, true);

                                    //CarsListView.ReloadData();
                                    .......
                                    */

                                    NSIndexPath[] path = new NSIndexPath[1];// NSIndexPath.FromRowSection(0, 0);
                                    path[0] = NSIndexPath.FromRowSection(d.rowNumber, 0);

                                    CarsListView.ReloadRows(path, UITableViewRowAnimation.None);
                                });
                            })).Start();
                        }
                    }
                }
            }

            /*
            new System.Threading.Thread(new System.Threading.ThreadStart(() =>
            {
                InvokeOnMainThread(() =>
                {
                    //if (carsData.Count > 0)
                    //{
                    //    NSIndexPath path = NSIndexPath.FromRowSection(0, 0);
                    //    var cell = (CarsCustomCell)CarsListView.CellAt(path);
                    //    cell.recentLap.Text = tickTickTimer(); //"KOTA";
                    //    Console.WriteLine(cell.recentLap.Text);
                    //    //cell.pgrView.SetProgress(data.duePercent, true);
                    //}

                    
                    CarsListView.ReloadData();
                    //CarsListView.ReloadRows(path, UITableViewRowAnimation.Automatic);
                    checkForTimerInvalidate();
                    
                });
            })).Start();
            */
        }

        void checkForTimerInvalidate()
        {
            int checkCarsProgressbar = 0;
            int timerRunningOnCars = 0;
            foreach (CarsInfo d in carsData)
            {
                if (d.duePercent > 1)
                {
                    checkCarsProgressbar += 1;
                    continue;
                }
                else
                {
                    break;
                }
            }
            if (checkCarsProgressbar == carsData.Count)
            {
                timer1.Stop();
            }

            foreach (CarsInfo d in carsData)
            {
                if (d.Lap1 != "")
                {
                    timerRunningOnCars += 1;
                }
            }
            if (checkCarsProgressbar == timerRunningOnCars)
            {
                timer1.Stop();
            }
        }

        //void checkIfAllTheCarsHas

        async Task AuthorizeCameraUse()
        {
            var authorizationStatus = AVCaptureDevice.GetAuthorizationStatus(AVMediaType.Video);

            if (authorizationStatus != AVAuthorizationStatus.Authorized)
            {
                await AVCaptureDevice.RequestAccessForMediaTypeAsync(AVMediaType.Video);
            }
            PHPhotoLibrary.RequestAuthorization(status =>
            {
                switch (status)
                {
                    case PHAuthorizationStatus.Restricted:
                    case PHAuthorizationStatus.Denied:
                        //Console.WriteLine("User Denied the access rights");
                        break;
                    case PHAuthorizationStatus.Authorized:
                        //Console.WriteLine("User agreed the access rights");
                        break;
                }
            });
        }

        partial void LapButtonClicked_TouchUpInside(UIButton sender)
        {
            if (showPreview)
            {
                captureImage();
            }
            else
            {
                var imageCaptureAlert = UIAlertController.Create("Error", "You cant capture image with camera preview set to OFF, please turn it ON", UIAlertControllerStyle.Alert);
                imageCaptureAlert.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel, alert => Console.WriteLine("Cancel was clicked")));
                PresentViewController(imageCaptureAlert, true, null);
            }
        }

        async void captureImage()
        {
            //CustomCameraViewController ccvc = Storyboard.InstantiateViewController("CustomCameraViewController") as CustomCameraViewController;
            //ccvc.ModalPresentationStyle = UIModalPresentationStyle.FullScreen;
            //Console.WriteLine("Lap button cliked at: " + DateTime.Now);
            //ccvc.trialCaptureImage();
            //startTime = DateTime.Now;
            //PresentViewController(ccvc, true, null);


            //startTime = DateTime.Now;
            if (startTimes.Count == 0)
            {
                startTime = DateTime.Now;
                startTimes.Add(startTime);
            }
            var videoConnection = stillImageOutput.ConnectionFromMediaType(AVMediaType.Video);
            var sampleBuffer = await stillImageOutput.CaptureStillImageTaskAsync(videoConnection);

            var jpegImageAsNsData = AVCaptureStillImageOutput.JpegStillToNSData(sampleBuffer);

            var img = UIImage.LoadFromData(jpegImageAsNsData);
            var jpegAsByteArray = jpegImageAsNsData.ToArray();

            /*
            // With some valid UIView *view:
            foreach (UIView subview in sclView.Subviews)
            {
                subview.RemoveFromSuperview();
            }
            //sclView.RemoveFromSuperview();
            */

            var compressedImgData = img.AsJPEG(.0f);//MaxResizeImage(img, sclView.Frame.Width, sclView.Frame.Height); Vish---
            var compressedImg = UIImage.LoadFromData(compressedImgData);

            fetchedImages.Add(img);//img 3rd April 2022
            imagesTakenTimes.Add(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
            /*
            scrlView(fetchedImages.Count);
            */

            //Adding car images to the scrollview
            var carImgView = new UIImageView(new CGRect(xPositionValue, 0, sclView.Frame.Width, sclView.Frame.Height));
            carImgView.Image = img;//compressedImg;
            carImgView.ContentMode = UIViewContentMode.ScaleAspectFill;
        
            carImgView.Tag = fetchedImages.Count - 1;
            sclView.AddSubview(carImgView);

            //Enabling drag action for all the images on the scroll view
            var dragAction = new UIDragInteraction(this);
            dragAction.Enabled = true;
            carImgView.AddInteraction(dragAction);
            carImgView.UserInteractionEnabled = true;

            sclView.ContentSize = new CGSize(sclView.Frame.Width * fetchedImages.Count, sclView.Frame.Height);

            //Updating the scrollview X position
            xPositionValue += (int)sclView.Frame.Width;

            //To show the latest image in the scrollview 
            CGRect frame = sclView.Frame;
            frame.X = frame.Width * (fetchedImages.Count - 1);
            frame.Y = 0;
            sclView.ScrollRectToVisible(frame, false);

            //carsCountLabel.Text = string.Format("{0}", fetchedImages.Count);
            NSNotificationCenter.DefaultCenter.PostNotificationName("ImagesCount", null);
            //---------
            /*
             * today
            if (fetchedImages.Count > 1)
            {
                if (carImgIndex == (fetchedImages.Count - 1))
                {
                    rightView.Hidden = true;
                    leftView.Hidden = false;
                }
                else if (carImgIndex == 0)
                {
                    rightView.Hidden = false;
                    leftView.Hidden = true;
                }
                else
                {
                    rightView.Hidden = false;
                    leftView.Hidden = false;
                }
            }
            else
            {
                rightView.Hidden = true;
                leftView.Hidden = true;
            }
            */

            _impactHeavyFeedbackGenerator = new UIImpactFeedbackGenerator(UIImpactFeedbackStyle.Heavy);
            _impactHeavyFeedbackGenerator.Prepare();
            _impactHeavyFeedbackGenerator.ImpactOccurred();
            _impactHeavyFeedbackGenerator.Dispose();

            selectedImgsForDeleting.Clear();
            refreshButton.Enabled = true;

            /*
            session = new AVCaptureSession();

            var viewLayer = View.Layer;
            videoPreviewLayer = new AVCaptureVideoPreviewLayer(session)
            {
                Frame = this.View.Frame
            };
            Console.WriteLine("Frame: " + videoPreviewLayer.Frame.Width + " Height: " + videoPreviewLayer.Frame.Height);
            //[captureVideoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
            videoPreviewLayer.VideoGravity = AVLayerVideoGravity.ResizeAspectFill;
            View.Layer.AddSublayer(videoPreviewLayer);

            var captureDevice = AVCaptureDevice.DefaultDeviceWithMediaType(AVMediaType.Video);
            ConfigureCameraForDevice(captureDevice);
            inputDevice = AVCaptureDeviceInput.FromDevice(captureDevice);
            session.AddInput(inputDevice);

            var dictionary = new NSMutableDictionary();
            dictionary[AVVideo.CodecKey] = new NSNumber((int)AVVideoCodec.JPEG);
            stillImageOutput = new AVCaptureStillImageOutput()
            {
                OutputSettings = new NSDictionary()
            };

            session.AddOutput(stillImageOutput);
            session.StartRunning();
             
             */


            //Console.WriteLine(DateTime.UtcNow.ToString());
            //Console.WriteLine(DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
            //Console.WriteLine(GetNISTDate().ToString("yyyy-MM-dd HH:mm:ss.SSSS"));
            //Console.WriteLine("------");
            //InternetTime.SNTPClient sntp = new InternetTime.SNTPClient("ntp1.ja.net");
            //sntp.Connect(false); // true to update local client clock
            //DateTime dt = sntp.DestinationTimestamp.AddMilliseconds(sntp.LocalClockOffset);
            //string timeStampNow = dt.ToString("dd/MM/yyyy HH:mm:ss.fff");

            /*
            img.SaveToPhotosAlbum((uiImage, nsError) =>
            {
                if (nsError != null)
                {
                    // do something about the error..
                    //Console.WriteLine("Error `" + nsError);
                }
                else
                {
                    // image should be saved
                    //Console.WriteLine("Saved");
                    _impactHeavyFeedbackGenerator = new UIImpactFeedbackGenerator(UIImpactFeedbackStyle.Heavy);
                    _impactHeavyFeedbackGenerator.Prepare();
                    _impactHeavyFeedbackGenerator.ImpactOccurred();
                }

            });
            
            showHideView.Hidden = true;
            refreshButton.Hidden = true;
            */
        }


        partial void UndoRedoButton_TouchUpInside(UIButton sender)
        {
            //calculateDueAlertPercentages();
            
            //Console.WriteLine(lastAddedCarInfoData.Keys);
            if (carsData.Count >= 1 && activites.Count > 0)
            {
                refreshButton.Enabled = true;
                CarsInfo lastItem = new CarsInfo();
                //lastItem = carsData[carsData.Count - 1];
                lastItem = carsData[activites[activites.Count - 1]];
                if (lastItem.Lap1 == "")
                {
                    carsData.RemoveAt(carsData.Count - 1);
                    deleteRecord(lastItem.rowNumber);
                }
                else if (lastItem.Lap4 != "")
                {
                    lastItem.Lap4 = "";
                    List<string> carTimes = carsInfoData[lastItem.CarName];
                    carTimes.RemoveAt(carTimes.Count - 1);
                    //carsData.Add(lastItem);
                    //carsData.RemoveAt(carsData.Count - 2);
                    //lastItem.RecentLapTime = string.Format("LAP {0}  Waiting..", carTimes.Count);// "LAP 4  Waiting..";

                    var lapNumber = carsLapData[lastItem.CarName].Count - 1;
                    //Console.WriteLine("Cars last index value: " + carsLapData[value.CarName][lapNumber - 1]);
                    var currentLapInfo = carsLapData[lastItem.CarName][lapNumber - 1];
                    var lapMillSec = currentLapInfo.Milliseconds.ToString("000").Substring(0, 2);
                    var formatedStr = string.Format("{0:D2}:{1:D2}.{2}", currentLapInfo.Minutes, currentLapInfo.Seconds, lapMillSec);
                    lastItem.RecentLapTime = string.Format("LAP {0}   {1}", lapNumber, formatedStr);

                    calculateAllTheLaps(carsInfoData);

                    TimeSpan bestTime = carsBestLapInfo[lastItem.CarName];
                    var bestLapMillSec = bestTime.Milliseconds.ToString("000").Substring(0, 2);
                    lastItem.BestLapTime = string.Format("Best - {0:D2}:{1:D2}.{2}", bestTime.Minutes, bestTime.Seconds, bestLapMillSec);//{2:D1}
                }
                else if (lastItem.Lap3 != "")
                {
                    lastItem.Lap3 = "";
                    List<string> carTimes = carsInfoData[lastItem.CarName];
                    carTimes.RemoveAt(carTimes.Count - 1);
                    //carsData.Add(lastItem);
                    //carsData.RemoveAt(carsData.Count - 2);
                    //calculateAllTheLaps(carsInfoData);
                    //lastItem.RecentLapTime = string.Format("LAP {0}  Waiting..", carTimes.Count);// "LAP 4  Waiting..";

                    var lapNumber = carsLapData[lastItem.CarName].Count - 1;
                    //Console.WriteLine("Cars last index value: " + carsLapData[value.CarName][lapNumber - 1]);
                    var currentLapInfo = carsLapData[lastItem.CarName][lapNumber - 1];
                    var lapMillSec = currentLapInfo.Milliseconds.ToString("000").Substring(0, 2);
                    var formatedStr = string.Format("{0:D2}:{1:D2}.{2}", currentLapInfo.Minutes, currentLapInfo.Seconds, lapMillSec);
                    lastItem.RecentLapTime = string.Format("LAP {0}   {1}", lapNumber, formatedStr);

                    calculateAllTheLaps(carsInfoData);

                    TimeSpan bestTime = carsBestLapInfo[lastItem.CarName];
                    var bestLapMillSec = bestTime.Milliseconds.ToString("000").Substring(0, 2);
                    lastItem.BestLapTime = string.Format("Best - {0:D2}:{1:D2}.{2}", bestTime.Minutes, bestTime.Seconds, bestLapMillSec);//{2:D1}
                }
                else if (lastItem.Lap2 != "")
                {
                    //lastItem.RecentLapTime = "LAP 2  Waiting..";
                    lastItem.Lap2 = "";
                    List<string> carTimes = carsInfoData[lastItem.CarName];
                    carTimes.RemoveAt(carTimes.Count - 1);
                    //carsData.Add(lastItem);
                    //carsData.RemoveAt(carsData.Count - 2);
                    //calculateAllTheLaps(carsInfoData);

                    //Test in on monday ***VISH***TASK1***
                    var lapNumber = carsLapData[lastItem.CarName].Count - 1;
                    //Console.WriteLine("Cars last index value: " + carsLapData[value.CarName][lapNumber - 1]);
                    var currentLapInfo = carsLapData[lastItem.CarName][lapNumber - 1];
                    var lapMillSec = currentLapInfo.Milliseconds.ToString("000").Substring(0, 2);
                    var formatedStr = string.Format("{0:D2}:{1:D2}.{2}", currentLapInfo.Minutes, currentLapInfo.Seconds, lapMillSec);
                    lastItem.RecentLapTime = string.Format("LAP {0}   {1}", lapNumber, formatedStr);

                    calculateAllTheLaps(carsInfoData);

                    TimeSpan bestTime = carsBestLapInfo[lastItem.CarName];
                    var bestLapMillSec = bestTime.Milliseconds.ToString("000").Substring(0, 2);
                    lastItem.BestLapTime = string.Format("Best - {0:D2}:{1:D2}.{2}", bestTime.Minutes, bestTime.Seconds, bestLapMillSec);//{2:D1}
                    //lastItem.RecentLapTime = string.Format("LAP {0}  Waiting..", carTimes.Count);// "LAP 4  Waiting..";
                }
                else if (lastItem.Lap1 != "")
                {
                    lastItem.Lap1 = "";
                    lastItem.RecentLapTime = "LAP 1  Waiting..";//"Recent
                                                                //--:--";
                    lastItem.BestLapTime = "Best - ";
                    List<string> carTimes = carsInfoData[lastItem.CarName];
                    carTimes.RemoveAt(carTimes.Count - 1);
                    //*****This condition is done***** #####Need to work on the other conditions such as lap2, lap3, lap4####
                    //carsInfoData[lastItem.CarName] = carTimes;

                    //carsData.Add(lastItem);
                    //carsData.RemoveAt(carsData.Count - 2);
                    //calculateAllTheLaps(carsInfoData);
                }

                fetchedImages.Add(backUpImages[backUpImages.Count - 1]);
                backUpImages.RemoveAt(backUpImages.Count - 1);
                imagesTakenTimes.Add(backUpImagesTakenTimes[backUpImagesTakenTimes.Count - 1]);
                backUpImagesTakenTimes.RemoveAt(backUpImagesTakenTimes.Count - 1);

                activites.RemoveAt(activites.Count - 1);

                if (activites.Count == 0)
                {
                    undoRedoButton.Enabled = false;
                }

                //sclView.RemoveFromSuperview();
                //scrlView(fetchedImages.Count);

                //fetchedImages.RemoveAt(carImgIndex);
 
                //sclView.BackgroundColor = UIColor.Red;

                //Adding car images to the scrollview
                var carImgView = new UIImageView(new CGRect(xPositionValue, 0, sclView.Frame.Width, sclView.Frame.Height));
                carImgView.Image = fetchedImages[fetchedImages.Count - 1];
                carImgView.Tag = fetchedImages.Count - 1;
                sclView.AddSubview(carImgView);

                //Enabling drag action for all the images on the scroll view
                var dragAction = new UIDragInteraction(this);
                dragAction.Enabled = true;
                carImgView.AddInteraction(dragAction);
                carImgView.UserInteractionEnabled = true;

                sclView.ContentSize = new CGSize(sclView.Frame.Width * fetchedImages.Count, sclView.Frame.Height);
                xPositionValue += (int)sclView.Frame.Width;
                //selectedCarsCount -= 1;

                CGRect frame = sclView.Frame;
                frame.X = frame.Width * (fetchedImages.Count - 1);
                frame.Y = 0;
                sclView.ScrollRectToVisible(frame, false);

                CarsListView.ReloadData();
            }
            else
            {
                undoRedoButton.Enabled = false;
                refreshButton.Enabled = false;
            }

            //carsCountLabel.Text = string.Format("{0}", fetchedImages.Count);
            NSNotificationCenter.DefaultCenter.PostNotificationName("ImagesCount", null);
        }

        partial void refreshButtonClicked(NSObject sender)
        {
            //foreach (var item in selectedImgsForDeleting)
            //{
            //    fetchedImages.RemoveAt(item);
            //}

            if (fetchedImages.Count > 0)
            {
                fetchedImages.RemoveAt(carImgIndex);
                xPositionValue -= (int)sclView.Frame.Width;

                var check = 0;
                foreach (UIView v in sclView.Subviews)
                {
                    if(v.Tag == carImgIndex && check == 0)
                    {
                        v.RemoveFromSuperview();
                        check = 1;
                        continue;
                    }
                    if (check == 1)
                    {
                        v.Frame = new CGRect(v.Frame.X - sclView.Frame.Width, 0, sclView.Frame.Width, sclView.Frame.Height);
                        v.Tag = v.Tag - 1;
                    }
                }
                check = 0;

                sclView.ContentSize = new CGSize(sclView.Frame.Width * fetchedImages.Count, sclView.Frame.Height);
                sclView.ScrollRectToVisible(new CGRect(), false);
                /*
                // With some valid UIView *view:
                foreach (UIView subview in sclView.Subviews)
                {
                    subview.RemoveFromSuperview();
                }
                sclView.RemoveFromSuperview();
                scrlView(fetchedImages.Count);
                */

                ///*
                // * today
                if (fetchedImages.Count == 0)
                {
                    refreshButton.Enabled = false;
                    //rightView.Hidden = true;
                    //leftImage.Hidden = true;
                    //return;
                }
            
            }
            else
            {
                refreshButton.Enabled = false;
            }
            //carsCountLabel.Text = string.Format("{0}", fetchedImages.Count); //heeeeeeeeee
            NSNotificationCenter.DefaultCenter.PostNotificationName("ImagesCount", null);
            /*
            exitTime = DateTime.Now;
            fetchPhotosInRange(startTime, exitTime);
            scrlView(fetchedImages.Count);
            startTimes.Clear();
            */
              
            if ((xPositionValue / sclView.Frame.Width) >= 1)
            {
                if (fetchedImages.Count == 1)
                {
                    rightView.Hidden = true;
                    leftView.Hidden = true;
                }
                else if (carImgIndex == ((xPositionValue / sclView.Frame.Width) - 1))
                {
                    rightView.Hidden = true;
                    leftView.Hidden = false;
                }
                else if (carImgIndex == 0)
                {
                    rightView.Hidden = false;
                    leftView.Hidden = true;
                }
                else
                {
                    rightView.Hidden = false;
                    leftView.Hidden = false;
                }
            }
            
        }

        partial void deleteAllButton(NSObject sender)
        {
            //Console.WriteLine("")
            //Create Alert
            var okCancelAlertController = UIAlertController.Create("Reset", "This action will remove all data from your device", UIAlertControllerStyle.Alert);

            //Add Actions
            okCancelAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, alert => deleteAllTables()));
            okCancelAlertController.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel, alert => Console.WriteLine("Cancel was clicked")));

            //Present Alert
            PresentViewController(okCancelAlertController, true, null);
        }
        partial void SettingsButton_TouchUpInside(UIButton sender)
        {
            //dropDatabaseTable();
            //carsData.Clear();
            //CarsListView.ReloadData();
            SettingsViewController svc = Storyboard.InstantiateViewController("SettingsViewController") as SettingsViewController;
            //NavigationController.PushViewController(svc, true);
            //svc.ModalPresentationStyle = UIModalPresentationStyle.FullScreen;
            PresentViewController(svc, true, null);
        }

        void ShowLapsListOfACar(NSNotification notification)
        {
            var selectedRow = notification.UserInfo["Row"];
            //Console.WriteLine("Selected row is: " + selectedRow.ToString());
            FullLapsList fll = Storyboard.InstantiateViewController("FullLapsList") as FullLapsList;
            //NavigationController.PushViewController(svc, true);
            //svc.ModalPresentationStyle = UIModalPresentationStyle.FullScreen;
            fll.fullLapData = carsLapData;
            fll.selectedCar = selectedRow.ToString();
            fll.actualCarsData = carsInfoData;
            PresentViewController(fll, true, null);
        }

        //MARK:- UIImageView Drag Delegate
        public UIDragItem[] GetItemsForBeginningSession(UIDragInteraction interaction, IUIDragSession session)
        {
            //Console.WriteLine("Car images index : " + carImgIndex);
            dragImage.Image = MaxResizeImage(fetchedImages[carImgIndex], 20, 20); //viiiiiiiii
            var image = dragImage.Image; //4 img1.image
            if (image == null)
                return new UIDragItem[] { };

            var provider = new NSItemProvider(image);
            var item = new UIDragItem(provider)
            {
                LocalObject = image
            };

            _impactHeavyFeedbackGenerator = new UIImpactFeedbackGenerator(UIImpactFeedbackStyle.Heavy);
            _impactHeavyFeedbackGenerator.Prepare();
            _impactHeavyFeedbackGenerator.ImpactOccurred();
            _impactHeavyFeedbackGenerator.Dispose();

            // If a non empty array is returned, dragging is enabled.
            return new UIDragItem[] { item };
        }

        [Export("dragInteraction:previewForLiftingItem:session:")]
        public UITargetedDragPreview GetPreviewForLiftingItem(UIDragInteraction interaction, UIDragItem item, IUIDragSession session)
        {
            var image = item.LocalObject as UIImage;
            if (image == null)
                return null;   

            var previewImageView = new UIImageView(image)
            {
                ContentMode = UIViewContentMode.ScaleAspectFit
            };
            var target = new UIDragPreviewTarget(sclView, sclView.Center);//5
            var newTarget = new UIDragPreviewTarget(sclView, new CGPoint(sclView.Frame.Width * carImgIndex + (sclView.Frame.Width/2), sclView.Frame.Width / 2)); //Remove this if drag image doesnt show good "+ (sclView.Frame.Width/2)"
            return new UITargetedDragPreview(previewImageView, new UIDragPreviewParameters(), newTarget);
        }
        
        //MARK:- Setup Scollview
        void setUpScrlView(int count)
        {
            var imageNames = new List<String>() { "car1", "car2", "car3", "car4", "car5", "car6", "car7", "car8" };
            
            ImagesScrlView.Scrolled += delegate
            {
                var pageWidth = ImagesScrlView.Frame.Width;
                var page = (int)Math.Floor((ImagesScrlView.ContentOffset.X - pageWidth / 2) / pageWidth) + 1;
                //pageControl.CurrentPage = page;
                carImgIndex = page;
                //dragImage = new UIImageView();
                //dragImage.Image = UIImage.FromBundle(imageNames[0]);//fetchedImages[page];
            };
            
            for (int i = 0; i < count; i++)
            {
                var carImgView = new UIImageView(new CGRect(i * ImagesScrlView.Frame.Width, 0, ImagesScrlView.Frame.Width, ImagesScrlView.Frame.Height));
                carImgView.Image = fetchedImages[i];
                ImagesScrlView.AddSubview(carImgView);

                var carTimeLbl = new UILabel(new CGRect(0, ImagesScrlView.Frame.Height - 30, ImagesScrlView.Frame.Width, 30));
                carTimeLbl.Text = imagesTakenTimes[i];
                carTimeLbl.TextColor = UIColor.White;
                carTimeLbl.TextAlignment = UITextAlignment.Right;
                carImgView.AddSubview(carTimeLbl);

                var dragAction = new UIDragInteraction(this);
                dragAction.Enabled = true;
                carImgView.AddInteraction(dragAction);
                carImgView.UserInteractionEnabled = true;
            }
            ImagesScrlView.ContentSize = new CGSize(ImagesScrlView.Frame.Width * count, ImagesScrlView.Frame.Height);
            //Console.WriteLine(carImgIndex);
        }

        //MARK:- Fetch photos
        private void fetchImagesFromPhotosApp()
        {
            DateTime objdate = DateTime.Today;
            //Console.WriteLine(String.Format("creationDate > " + objdate + "AND creationDate < " + objdate));

            fetchPhotosInRange(DateTime.Parse("14-10-2020 11:00:00"), DateTime.Parse("17-10-2020 18:38:00"));//dd-mm-yyyy HH:mm:ss
        }

        public void fetchPhotosInRange(DateTime startDate, DateTime endDate)
        {
            var imgManager = new PHImageManager();

            var reqOptions = new PHImageRequestOptions();
            reqOptions.Synchronous = true;
            reqOptions.NetworkAccessAllowed = true;

            var fetchOptions = new PHFetchOptions();
            fetchedImages.Clear();
            imagesTakenTimes.Clear();

            var fetchResults = PHAsset.FetchAssets(PHAssetMediaType.Image, fetchOptions);

            if (fetchResults.Count > 0)
            {
                /*
                foreach (var item in fetchResults)
                {
                    Console.WriteLine((item as PHAsset).MediaSubtypes);
                }
                */
                for (var i = 0; i < fetchResults.Count; i++)
                {
                    var asset = fetchResults.ObjectAt(i) as PHAsset;
                    var time = asset.CreationDate;
                    //Console.WriteLine("Time taken: " + time);
                    var val1 = ConvertNsDateToDateTime(time);

                    int result = DateTime.Compare(val1, startDate);
                    int result1 = DateTime.Compare(val1, endDate);
                    if (result >= 0 && result1 <= 0)
                    {
                        var formatter = new NSDateFormatter();
                        formatter.DateFormat = "yyyy-MM-dd HH:mm:ss.SSSS";

                        var str = formatter.ToString(time);
                        var yourDate = DateTime.Parse(str);
                        //var finalStr = yourDate.ToString();
                        var finalStr = formatter.ToString(time);
                        imagesTakenTimes.Add(finalStr);

                        imgManager.RequestImageData(asset, reqOptions,
                            (imgdata, dataUti, orientation, info) =>
                            {
                                var imageData = imgdata;
                                if (imageData != null)
                                {
                                    var img = new UIImage(data: imgdata);
                                    fetchedImages.Add(img);
                                    //imgs.Add(img);
                                }
                            });
                    }
                }
                //Console.WriteLine(imagesTakenTimes.Count);
                //Console.WriteLine(fetchedImages.Count);
            }
        }
        /*
        public DateTime ConvertNsDateToDateTime(NSDate date)
        {
            DateTime newDate = TimeZone.CurrentTimeZone.ToLocalTime(
                new DateTime(2001, 1, 1, 0, 0, 0));
            return newDate.AddSeconds(date.SecondsSinceReferenceDate);
        }
        */
        public DateTime ConvertNsDateToDateTime(Foundation.NSDate date)
        {
            DateTime reference = new DateTime(2001, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var utcDateTime = reference.AddSeconds(date.SecondsSinceReferenceDate);
            return utcDateTime.ToLocalTime();
        }

        public void getExistingData(NSNotification notification)
        {
            //Console.WriteLine("Testing");
            //Console.WriteLine("Selected data: " + AppDelegate.selectedCarData);
            existingCarName = AppDelegate.selectedCarData["changeCarName"].CarName;

            UIAlertController alert = UIAlertController.Create("Change Car Name (Maximum of 10 characters longs)", "", UIAlertControllerStyle.Alert);

            alert.AddAction(UIAlertAction.Create("Save", UIAlertActionStyle.Default, action => {
                updateCarName(alert.TextFields[0].Text);
            }));

            alert.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel, null));
            alert.AddTextField((field) => {
                field.Placeholder = "Enter Car name";
                field.Text = existingCarName;

                field.EditingChanged += (object sender, EventArgs e) =>
                {
                    if (field.Text.Length > 10)
                    {
                        var trimText = field.Text.Remove(10, 1);
                        field.Text = trimText;
                    }
                };
            });
            PresentViewController(alert, true, null);
        }

        public void updateCarName(string updatedName)
        {
            //Console.WriteLine(updatedName);
            //Dictionary<string, List<string>> lastValue = AppDelegate.carsInfoDataDelegate

            CarsInfo info = new CarsInfo();
            info = AppDelegate.selectedCarData["changeCarName"];

            CarsInfo previous = GetSingleRecord(info.rowNumber);

            for (var i = 0; i < carsData.Count; i++)
            {
                if (carsData[i].CarName.Equals(info.CarName) && carsData[i].rowNumber == info.rowNumber)
                {
                    carsData[i].CarName = updatedName;
                    previous.CarName = updatedName;

                    //carsInfoData[updatedName] = carsInfoData[existingCarName];
                    //carsLapData[updatedName] = carsLapData[existingCarName];
                    //carsBestLapInfo[updatedName] = carsBestLapInfo[existingCarName];

                    //Dictionary<string, List<string>> carsInfoData = new Dictionary<string, List<string>>();
                    //Dictionary<string, TimeSpan> carsBestLapInfo = new Dictionary<string, TimeSpan>();
                    //Dictionary<string, List<TimeSpan>> carsLapData = new Dictionary<string, List<TimeSpan>>();

                    using (var connection = new SQLite.SQLiteConnection(pathToDatabase))
                    {
                        connection.Update(previous);//CreateTable<CarsInfo>();
                    }
                    break;
                }
            }

            if (carsInfoData.ContainsKey(existingCarName))
            {
                carsInfoData[info.CarName] = carsInfoData[existingCarName];
                carsInfoData.Remove(existingCarName);
            }

            if (carsLapData.ContainsKey(existingCarName))
            {
                carsLapData[info.CarName] = carsLapData[existingCarName];
                carsLapData.Remove(existingCarName);
            }
           
            if (carsBestLapInfo.ContainsKey(existingCarName))
            {
                carsBestLapInfo[info.CarName] = carsBestLapInfo[existingCarName];
                carsBestLapInfo.Remove(existingCarName);
            }
            
            CarsListView.ReloadData();

            //Console.WriteLine("Updated CarsData: " + carsData);
            //Console.WriteLine("----------------------------");
            //Console.WriteLine("Cars Lap Data: " + carsLapData);
            //Console.WriteLine("----------------------------");
            //Console.WriteLine("Cars Best Lap Data: " + carsBestLapInfo);
        }


        UIImageView carImage = new UIImageView();
        UIView popupView = new UIView();
        UIScrollView imageScrlView = new UIScrollView();
        UIView cropView = new UIView();
        public void getCroppedImage(NSNotification notification)
        {
            for (var i = 0; i < carsData.Count; i++)
            {
                if (carsData[i].CarName.Equals(AppDelegate.selectedCarData["changeCarName"].CarName))
                {
                    //carsData[i].CarImage = croppedImage;
                    /*
                        UIImage c = UIImage.FromImage(croppedCGImage);
                        carsData[i].CarImage = UIImage.FromImage(croppedCGImage);
                    */
                    UIImage img = AppDelegate.crpImage;//notification.UserInfo.ObjectForKey(new NSString("cimage"));//notification.UserInfo["cimage"];
                    //carsData[i].CarImage = img;//notification.UserInfo["cim age"];
                    using (NSData imageData = img.AsPNG())
                    {
                        Byte[] myByteArray = new Byte[imageData.Length];
                        carsData[i].CarImage = imageData.ToArray();

                        //System.Runtime.InteropServices.Marshal.Copy(imageData.Bytes, myByteArray, 0, Convert.ToInt32(imageData.Length));
                    }
                    CarsListView.ReloadData();
                }
            }
        }

        public void getExistingImageData(NSNotification notification)
        {
            //Console.WriteLine("Selected data: " + AppDelegate.selectedCarData);
            //existingImage = AppDelegate.selectedCarData["changeCarName"].CarImage;
            existingImage = UIImage.LoadFromData(NSData.FromArray(AppDelegate.selectedCarData["changeCarName"].CarImage));

            var cropVC = new TOCropViewController(TOCropViewCroppingStyle.Default, existingImage);
            cropVC.Delegate = new MyCropVCDelegate();
            this.PresentViewController(cropVC, true, null);

        }

        CGRect actualImageFrame(UIImage carImg)
        {
            var imageViewSize = carImg.Size;
            var imageSize = carImg.Size;
            if (imageSize != null)
            {
                var imageRatio = imageSize.Width / imageSize.Height;
                var imageViewRatio = imageViewSize.Width / imageViewSize.Height;
                if (imageRatio < imageViewRatio)
                {
                    var scaleFactor = imageViewSize.Height / imageSize.Height;
                    var width = imageSize.Width * scaleFactor;
                    var topleftX = (imageViewSize.Width - width) * 0.5;
                    return new CGRect(topleftX, 0, width, imageViewSize.Height);
                }
                else
                {
                    var scaleFactor = imageViewSize.Width / imageSize.Width;
                    var height = imageSize.Height * scaleFactor;
                    var topLeftY = (imageViewSize.Height - height) * 0.5;
                    return new CGRect(0, topLeftY, imageViewSize.Width, height);
                }
            }
            return new CGRect(0, 0, 0, 0);
        }

        CGRect cropArea()
        {
            var factor = carImage.Image.Size.Width / popupView.Frame.Width;
            var scale = 1 / imageScrlView.ZoomScale;
            var imageFrame = actualImageFrame(carImage.Image);
            var x = (imageScrlView.ContentOffset.X + cropView.Frame.Location.X - imageFrame.Location.X) * scale * factor;
            var y = (imageScrlView.ContentOffset.Y + cropView.Frame.Location.Y - imageFrame.Location.Y) * scale * factor;

            var width = cropView.Frame.Size.Width * scale * factor;
            var height = cropView.Frame.Size.Height * scale * factor;
            UIImage v = i.CenterCrop(new UIImage());
            return new CGRect(x, y, width, height);
        }
        
        //Crops an image to even width and height
        public UIImage CenterCrop(UIImage originalImage)
        {
            // Use smallest side length as crop square length
            double squareLength = Math.Min(originalImage.Size.Width, originalImage.Size.Height);

            nfloat x, y;
            x = (nfloat)((originalImage.Size.Width - squareLength) / 2.0);
            y = (nfloat)((originalImage.Size.Height - squareLength) / 2.0);

            //This Rect defines the coordinates to be used for the crop
            CGRect croppedRect = CGRect.FromLTRB(x, y, x + (nfloat)squareLength, y + (nfloat)squareLength);

            // Center-Crop the image
            UIGraphics.BeginImageContextWithOptions(croppedRect.Size, false, originalImage.CurrentScale);
            originalImage.Draw(new CGPoint(-croppedRect.X, -croppedRect.Y));
            UIImage croppedImage = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();

            return croppedImage;
        }

        public void deleteSelectedImages(NSNotification notification)
        {
            if(selectedImgsForDeleting.Count > 0)
            {
                //Enable delete button
                refreshButton.Hidden = false;
            }
            else
            {
                //hide delete button
                refreshButton.Hidden = true;
            }
        }
        
        public void ShowCamPreview(NSNotification notification)
        {
            var value = notification.UserInfo["showCamPreview"];
            Console.WriteLine(value);
            if (value.ToString() == "1")
            {
                Console.WriteLine("True");
                livePreviewView.Hidden = false;
            }
            else
            {
                Console.WriteLine("False");
                livePreviewView.Hidden = false; //true; v1.17
            }
        }

        public void SetupLiveCameraStream()
        {
            /*
            UIScrollView s = new UIScrollView();
            //s.Frame = new CGRect(
            //    showCamButton.Frame.X + showCamButton.Frame.Size.Height,
            //    livePreviewView.Frame.Y,
            //    showCamButton.Frame.Width + refreshButton.Frame.Width + settingsButton.Frame.Width + 20,
            //    settingsButton.Frame.Size.Width + settingsButton.Frame.X
            //    );
            
            s.Frame = livePreviewView.Frame;

            s.BackgroundColor = UIColor.Red;
            topView.AddSubview(s);
            */

            livePreviewView.RemoveFromSuperview();
            session = new AVCaptureSession();

            var viewLayer = View.Layer;
            videoPreviewLayer = new AVCaptureVideoPreviewLayer(session);
            //{
            //    Frame = livePreviewView.Frame
            //};
            //Console.WriteLine("Frame: " + videoPreviewLayer.Frame.Width + " Height: " + videoPreviewLayer.Frame.Height);
            //[captureVideoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
            videoPreviewLayer.VideoGravity = AVLayerVideoGravity.ResizeAspectFill; //.Resize;
            videoPreviewLayer.Frame = new CGRect(livePreviewView.Frame.X-25, livePreviewView.Frame.Y, livePreviewView.Bounds.Width, livePreviewView.Bounds.Height);
            livePreviewView.Layer.AddSublayer(videoPreviewLayer);

            livePreviewView.Frame = videoPreviewLayer.Frame;

            /*
            // Trying for Zooming Preview
            // Not working
            livePreviewView.UserInteractionEnabled = true;

            UITapGestureRecognizer ges = new UITapGestureRecognizer(ZoomCapture);
            ges.NumberOfTapsRequired = 2;
            livePreviewView.AddGestureRecognizer(ges);
            //
            */

            topView.AddSubview(livePreviewView);


            var captureDevice = AVCaptureDevice.DefaultDeviceWithMediaType(AVMediaType.Video);
            ConfigureCameraForDevice(captureDevice);
            inputDevice = AVCaptureDeviceInput.FromDevice(captureDevice);
            session.AddInput(inputDevice);

            var dictionary = new NSMutableDictionary();
            dictionary[AVVideo.CodecKey] = new NSNumber((int)AVVideoCodec.JPEG);
            stillImageOutput = new AVCaptureStillImageOutput()
            {
                OutputSettings = new NSDictionary()
            };

            session.AddOutput(stillImageOutput);
            session.StartRunning();

            //livePreviewView.Layer.BorderColor = UIColor.Red.CGColor;
            //livePreviewView.Layer.BorderWidth = 5.0f;

        }

        /*
         * Not Working
        void ZoomCapture(UITapGestureRecognizer taped)
        {
            videoPreviewLayer.VideoGravity = AVLayerVideoGravity.Resize;
        }
        */


        partial void showHideCameraPreview(NSObject sender)
        {
            showPreview = !showPreview;

            if (showPreview)
            {
                showCamButton.SetImage(UIImage.FromBundle("cameraShow"), forState: UIControlState.Normal);
                session.StartRunning();
                livePreviewView.Hidden = false;
            }
            else
            {
                showCamButton.SetImage(UIImage.FromBundle("CameraHide"), forState: UIControlState.Normal);
                session.StopRunning();
                livePreviewView.Hidden = true;
            }
        }

        void zoomCameraFeedView()
        {
            //Not working
            CGAffineTransform transform = CGAffineTransform.MakeIdentity();
            transform.Scale(2f, 2f);
            livePreviewView.Transform = transform;
        }


        void ConfigureCameraForDevice(AVCaptureDevice device)
        {
            var error = new NSError();
            if (device.IsFocusModeSupported(AVCaptureFocusMode.ContinuousAutoFocus))
            {
                device.LockForConfiguration(out error);
                device.FocusMode = AVCaptureFocusMode.ContinuousAutoFocus;
                device.UnlockForConfiguration();
            }
            else if (device.IsExposureModeSupported(AVCaptureExposureMode.ContinuousAutoExposure))
            {
                device.LockForConfiguration(out error);
                device.ExposureMode = AVCaptureExposureMode.ContinuousAutoExposure;
                device.UnlockForConfiguration();
            }  
            else if (device.IsWhiteBalanceModeSupported(AVCaptureWhiteBalanceMode.ContinuousAutoWhiteBalance))
            {
                device.LockForConfiguration(out error);
                device.WhiteBalanceMode = AVCaptureWhiteBalanceMode.ContinuousAutoWhiteBalance;
                device.UnlockForConfiguration();
            }
        }

        async partial void OneMoreLapsButtonAction(NSObject sender)
        {
            var videoConnection = stillImageOutput.ConnectionFromMediaType(AVMediaType.Video);
            var sampleBuffer = await stillImageOutput.CaptureStillImageTaskAsync(videoConnection);

            var jpegImageAsNsData = AVCaptureStillImageOutput.JpegStillToNSData(sampleBuffer);

            var img = UIImage.LoadFromData(jpegImageAsNsData);
            var jpegAsByteArray = jpegImageAsNsData.ToArray();

            img.SaveToPhotosAlbum((uiImage, nsError) =>
            {
                if (nsError != null)
                {
                    // do something about the error..
                    //Console.WriteLine("Error `" + nsError);
                }
                else
                {
                    // image should be saved
                    //Console.WriteLine("Saved");
                    _impactHeavyFeedbackGenerator = new UIImpactFeedbackGenerator(UIImpactFeedbackStyle.Heavy);
                    _impactHeavyFeedbackGenerator.Prepare();
                    _impactHeavyFeedbackGenerator.ImpactOccurred();
                }

            });
        }

        partial void stopButtonAction(NSObject sender)
        {
            showHideView.Hidden = true;
            exitTime = DateTime.Now;
            fetchPhotosInRange(startTime, exitTime);
            scrlView(fetchedImages.Count);
        }

        // resize the image to be contained within a maximum width and height, keeping aspect ratio
        public UIImage MaxResizeImage(UIImage sourceImage, float maxWidth, float maxHeight)
        {
            var sourceSize = sourceImage.Size;
            var maxResizeFactor = Math.Max(maxWidth / sourceSize.Width, maxHeight / sourceSize.Height);
            if (maxResizeFactor > 1) return sourceImage;
            var width = (float)maxResizeFactor * sourceSize.Width;
            var height = maxResizeFactor * sourceSize.Height;
            UIGraphics.BeginImageContext(new CGSize(width, height));
            sourceImage.Draw(new CGRect(0, 0, width, height));
            var resultImage = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();
            return resultImage;
        }
    }
}

/*
 var cropArea:CGRect{
     get{
          let factor = imageView.image!.size.width/view.frame.width
          let scale = 1/scrollView.zoomScale
          let imageFrame = imageView.imageFrame()
          let x = (scrollView.contentOffset.x + cropAreaView.frame.origin.x - imageFrame.origin.x) * scale * factor
          let y = (scrollView.contentOffset.y + cropAreaView.frame.origin.y - imageFrame.origin.y) * scale * factor
          let width = cropAreaView.frame.size.width * scale * factor
          let height = cropAreaView.frame.size.height * scale * factor
          return CGRect(x: x, y: y, width: width, height: height)
     }
}
  
extension UIImageView{
   func imageFrame()->CGRect{
     let imageViewSize = self.frame.size
     guard let imageSize = self.image?.size else{return CGRect.zero}
     let imageRatio = imageSize.width / imageSize.height
     let imageViewRatio = imageViewSize.width / imageViewSize.height
     if imageRatio < imageViewRatio {
        let scaleFactor = imageViewSize.height / imageSize.height
        let width = imageSize.width * scaleFactor
        let topLeftX = (imageViewSize.width - width) * 0.5
        return CGRect(x: topLeftX, y: 0, width: width, height: imageViewSize.height)
     }else{
        let scalFactor = imageViewSize.width / imageSize.width
        let height = imageSize.height * scalFactor
        let topLeftY = (imageViewSize.height - height) * 0.5
        return CGRect(x: 0, y: topLeftY, width: imageViewSize.width, height: height)
     }
   }
}
*/