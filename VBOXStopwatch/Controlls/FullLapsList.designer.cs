// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace VBOXStopwatch
{
	[Register ("FullLapsList")]
	partial class FullLapsList
	{
		[Outlet]
		UIKit.UITableView lapsInfoTblView { get; set; }

		[Outlet]
		UIKit.UILabel titleLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (titleLabel != null) {
				titleLabel.Dispose ();
				titleLabel = null;
			}

			if (lapsInfoTblView != null) {
				lapsInfoTblView.Dispose ();
				lapsInfoTblView = null;
			}
		}
	}
}
