// This file has been autogenerated from a class added in the UI designer.

using System;
using System.Collections.Generic;
using Foundation;
using UIKit;
using VBOXStopwatch.Models;
using VBOXStopwatch.UserControlls;

namespace VBOXStopwatch
{
	public partial class FullLapsList : UIViewController
	{
		public Dictionary<string, List<TimeSpan>> fullLapData;
		public Dictionary<string, List<string>> actualCarsData;
        public int bestLap;

		public string selectedCar;
		List<string> data = new List<string>();

        CarLapDetails carsLD = new CarLapDetails();
        TimeSpan bestTime = new TimeSpan();

        public FullLapsList(IntPtr handle) : base(handle)
		{
		}

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            titleLabel.Text = selectedCar;

			var allLaps = fullLapData[selectedCar];
			int i = 1;
			foreach(TimeSpan t in allLaps)
            {
				var formatedStr = string.Format("{0:D2}:{1:D2}.{2:D2}", t.Minutes, t.Seconds, t.Milliseconds);
				string lap = "Lap " + i + ":  " + formatedStr;
				data.Add(lap);
				i++;
            }

			//lapsInfoTblView.Source = new FullLapsListDataSource(data);

            fetchAllData();
        }

        void fetchAllData()
        {
            //carsLD.CarName = selectedCar;
            calculateAllTheLaps();
            //var bestLapMillSec = bestTime.Milliseconds.ToString("000").Substring(0, 2);
            //carsLD.bestLap = string.Format("Best - {0:D2}:{1:D2}.{2}", bestTime.Minutes, bestTime.Seconds, bestLapMillSec);//{2:D1}

            List<string> lapsD = new List<string>();
            var allLaps = fullLapData[selectedCar];
            int i = 1;
            foreach (TimeSpan t in allLaps)
            {
                var formatedStr = string.Format("{0:D2}:{1:D2}.{2:D4}", t.Minutes, t.Seconds, t.Milliseconds);
                var lapDiff = t - bestTime;
                var lap1DiffMilliSec = lapDiff.Milliseconds.ToString("000").Substring(0, 2);

                var lap1Str = string.Format("{0:D1}.{1:D2}", lapDiff.Seconds, lapDiff.Milliseconds);

                if (float.Parse(lap1Str) >= 0.00)
                {
                    lap1Str = string.Format("+{0:D1}.{1}", lapDiff.Seconds, lap1DiffMilliSec);
                }
                else
                {
                    lap1Str = string.Format("-{0:D1}.{1}", lapDiff.Seconds, lap1DiffMilliSec);
                }

                string lap = "Lap " + i + ":  " + formatedStr + "      " + lap1Str;
                lapsD.Add(lap);
                i++;
            }

            lapsInfoTblView.Source = new FullLapsListDataSource(lapsD);
        }

        void calculateAllTheLaps()
        {
            List<string> times = actualCarsData[selectedCar];
            for (int i = 0; i < times.Count; i++)
            {
                if(i+1 < times.Count)
                {
                    TimeSpan diff = Convert.ToDateTime(times[i + 1]) - Convert.ToDateTime(times[i]);
                    if (diff < bestTime)
                    {
                        bestTime = diff;
                        bestLap = i + 1;
                    }
                    if (i == 0)
                    {
                        bestTime = diff;
                        bestLap = 1;
                    }
                }
            }
        }

        public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}
