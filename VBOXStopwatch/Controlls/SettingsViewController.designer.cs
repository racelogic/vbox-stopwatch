// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace VBOXStopwatch
{
	[Register ("SettingsViewController")]
	partial class SettingsViewController
	{
		[Outlet]
		UIKit.UILabel appVersionNumber { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIKit.UIButton backBtn { get; set; }

		[Outlet]
		UIKit.UIButton closeBtn { get; set; }

		[Outlet]
		UIKit.UIButton deleteAllBtn { get; set; }

		[Outlet]
		UIKit.UIButton newSessionBtn { get; set; }

		[Outlet]
		UIKit.UISwitch previewSwitch { get; set; }

		[Outlet]
		UIKit.UIButton supportBtn { get; set; }

		[Action ("BackBtn_TouchUpInside:")]
		partial void BackBtn_TouchUpInside (UIKit.UIButton sender);

		[Action ("CameraPreview:")]
		partial void CameraPreview (Foundation.NSObject sender);

		[Action ("closeBtnCliked:")]
		partial void closeBtnCliked (Foundation.NSObject sender);

		[Action ("deleteAllBtnClicked:")]
		partial void deleteAllBtnClicked (Foundation.NSObject sender);

		[Action ("newSessionClicked:")]
		partial void newSessionClicked (Foundation.NSObject sender);

		[Action ("suppportBtnClicked:")]
		partial void suppportBtnClicked (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (appVersionNumber != null) {
				appVersionNumber.Dispose ();
				appVersionNumber = null;
			}

			if (backBtn != null) {
				backBtn.Dispose ();
				backBtn = null;
			}

			if (closeBtn != null) {
				closeBtn.Dispose ();
				closeBtn = null;
			}

			if (deleteAllBtn != null) {
				deleteAllBtn.Dispose ();
				deleteAllBtn = null;
			}

			if (previewSwitch != null) {
				previewSwitch.Dispose ();
				previewSwitch = null;
			}

			if (supportBtn != null) {
				supportBtn.Dispose ();
				supportBtn = null;
			}

			if (newSessionBtn != null) {
				newSessionBtn.Dispose ();
				newSessionBtn = null;
			}
		}
	}
}
