// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace VBOXStopwatch.Controlls
{
	[Register ("LapDetailsViewController")]
	partial class LapDetailsViewController
	{
		[Outlet]
		UIKit.UILabel carNameLable { get; set; }

		[Outlet]
		UIKit.UITableView lapsListView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (carNameLable != null) {
				carNameLable.Dispose ();
				carNameLable = null;
			}

			if (lapsListView != null) {
				lapsListView.Dispose ();
				lapsListView = null;
			}
		}
	}
}
