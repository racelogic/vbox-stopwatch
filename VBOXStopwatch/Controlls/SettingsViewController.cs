using Foundation;
using System;
using UIKit;

namespace VBOXStopwatch
{
    public partial class SettingsViewController : UIViewController
    {
        public SettingsViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            var str = NSBundle.MainBundle.InfoDictionary["CFBundleVersion"].Description;
            appVersionNumber.Text = "1." + str; //$(CURRENT_PROJECT_VERSION)

            bool ToggleSwitch = NSUserDefaults.StandardUserDefaults.BoolForKey("ToggleSwitch");
            
            previewSwitch.SetState(ToggleSwitch, true);
        }

        partial void BackBtn_TouchUpInside(UIButton sender)
        {
            Console.WriteLine("Testing");
            //ViewController vc = Storyboard.InstantiateViewController("ViewController") as ViewController;
            //NavigationController.PushViewController(vc, true);
            //this.NavigationController.PopViewController(true);
            //NavigationController.DismissViewController(true, null);
            this.DismissViewController(true, null);
        }

        partial void closeBtnCliked(NSObject sender)
        {
            Console.WriteLine("Testing");
            this.DismissViewController(true, null);
        }

        void deleteAll()
        {
            NSNotificationCenter.DefaultCenter.PostNotificationName("ResetData", null);
            this.DismissViewController(true, null);
        }

        void clearSessionData()
        {
            var passdata = new NSDictionary(new NSString("newSession"), 1);

            NSNotificationCenter.DefaultCenter.PostNotificationName("ClearSession", null, passdata);
            this.DismissViewController(true, null);
        }

        partial void CameraPreview(NSObject sender)
        {
            Boolean value = previewSwitch.On;
            
            var passdata = new NSDictionary(new NSString("showCamPreview"), value);
    
            NSNotificationCenter.DefaultCenter.PostNotificationName("ShowPreview", null, passdata);

            NSUserDefaults.StandardUserDefaults.SetBool(value, "ToggleSwitch");
            NSUserDefaults.StandardUserDefaults.Synchronize();
        }

        partial void deleteAllBtnClicked(NSObject sender)
        {
            var okCancelAlertController = UIAlertController.Create("Reset Data", "This action will remove all data from your device and start a new session", UIAlertControllerStyle.Alert);

            //Add Actions
            okCancelAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, alert => deleteAll()));
            okCancelAlertController.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel, alert => Console.WriteLine("Cancel was clicked")));

            //Present Alert
            PresentViewController(okCancelAlertController, true, null);
        }

        partial void suppportBtnClicked(NSObject sender)
        {
            UIApplication.SharedApplication.OpenUrl(new NSUrl("https://vboxmotorsport.co.uk/index.php/en/"));
        }

        partial void newSessionClicked(NSObject sender)
        {
            clearSessionData();
        }
    }
}