#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#pragma clang diagnostic ignored "-Wtypedef-redefinition"
#pragma clang diagnostic ignored "-Wobjc-designated-initializers"
#pragma clang diagnostic ignored "-Wunguarded-availability-new"
#define DEBUG 1
#include <stdarg.h>
#include <objc/objc.h>
#include <objc/runtime.h>
#include <objc/message.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>
#import <Photos/Photos.h>
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>

@class SceneDelegate;
@class AppDelegate;
@protocol TOCropViewControllerDelegate;
@class TOCropViewControllerDelegate;
@class VBOXStopwatch_MyCropVCDelegate;
@class CarsCustomCell;
@class UITableViewSource;
@class VBOXStopwatch_CarsListViewDataSource;
@class FullLapsList;
@class VBOXStopwatch_UserControlls_FullLapsListDataSource;
@class LapDetailsViewController;
@class ViewController;
@class SettingsViewController;
@class CustomCameraViewController;
@class Foundation_NSDispatcher;
@class __MonoMac_NSActionDispatcher;
@class __MonoMac_NSSynchronizationContextDispatcher;
@class Foundation_NSAsyncDispatcher;
@class __MonoMac_NSAsyncActionDispatcher;
@class __MonoMac_NSAsyncSynchronizationContextDispatcher;
@class Foundation_InternalNSNotificationHandler;
@class UIKit_UIControlEventProxy;
@class __MonoTouch_UIImageStatusDispatcher;
@class __NSObject_Disposer;
@class __UIGestureRecognizerToken;
@class __UITapGestureRecognizer;
@class UIKit_UIView_UIViewAppearance;
@class UIKit_UIScrollView__UIScrollViewDelegate;
@class UIKit_UIScrollView_UIScrollViewAppearance;
@class TOActivityCroppedImageProvider;
@class TOCroppedImageAttributes;
@class TOCropViewController;
@class TOCropViewControllerTransitioning;
@protocol TOCropViewDelegate;
@class TOCropViewDelegate;
@class Xamarin_TOCropView_TOCropOverlayView_TOCropOverlayViewAppearance;
@class TOCropOverlayView;
@class Xamarin_TOCropView_TOCropScrollView_TOCropScrollViewAppearance;
@class TOCropScrollView;
@class Xamarin_TOCropView_TOCropToolbar_TOCropToolbarAppearance;
@class TOCropToolbar;
@class Xamarin_TOCropView_TOCropView_TOCropViewAppearance;
@class TOCropView;

@interface SceneDelegate : UIResponder<UIWindowSceneDelegate> {
}
	@property (nonatomic, assign) UIWindow * window;
	-(void) release;
	-(id) retain;
	-(GCHandle) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (GCHandle) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(UIWindow *) window;
	-(void) setWindow:(UIWindow *)p0;
	-(void) scene:(UIScene *)p0 willConnectToSession:(UISceneSession *)p1 options:(UISceneConnectionOptions *)p2;
	-(void) sceneDidDisconnect:(UIScene *)p0;
	-(void) sceneDidBecomeActive:(UIScene *)p0;
	-(void) sceneWillResignActive:(UIScene *)p0;
	-(void) sceneWillEnterForeground:(UIScene *)p0;
	-(void) sceneDidEnterBackground:(UIScene *)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface AppDelegate : UIResponder<UIApplicationDelegate> {
}
	@property (nonatomic, assign) UIWindow * window;
	-(void) release;
	-(id) retain;
	-(GCHandle) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (GCHandle) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(UIWindow *) window;
	-(void) setWindow:(UIWindow *)p0;
	-(BOOL) application:(UIApplication *)p0 didFinishLaunchingWithOptions:(NSDictionary *)p1;
	-(UISceneConfiguration *) application:(UIApplication *)p0 configurationForConnectingSceneSession:(UISceneSession *)p1 options:(UISceneConnectionOptions *)p2;
	-(void) application:(UIApplication *)p0 didDiscardSceneSessions:(NSSet <UISceneSession *>*)p1;
	-(void) applicationWillTerminate:(UIApplication *)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@protocol TOCropViewControllerDelegate
	@optional -(void) cropViewController:(id)p0 didCropImageToRect:(CGRect)p1 angle:(NSInteger)p2;
	@optional -(void) cropViewController:(id)p0 didCropToImage:(UIImage *)p1 withRect:(CGRect)p2 angle:(NSInteger)p3;
	@optional -(void) cropViewController:(id)p0 didCropToCircularImage:(UIImage *)p1 withRect:(CGRect)p2 angle:(NSInteger)p3;
	@optional -(void) cropViewController:(id)p0 didFinishCancelled:(BOOL)p1;
@end

@interface TOCropViewControllerDelegate : NSObject<TOCropViewControllerDelegate> {
}
	-(id) init;
@end

@interface VBOXStopwatch_MyCropVCDelegate : NSObject<TOCropViewControllerDelegate> {
}
	-(void) release;
	-(id) retain;
	-(GCHandle) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (GCHandle) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) cropViewController:(id)p0 didCropImageToRect:(CGRect)p1 angle:(NSInteger)p2;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface CarsCustomCell : UITableViewCell {
}
	@property (nonatomic, assign) UILabel * bestLap;
	@property (nonatomic, assign) UIImageView * carImage;
	@property (nonatomic, assign) UILabel * carName;
	@property (nonatomic, assign) UIView * carnameVw;
	@property (nonatomic, assign) UILabel * cNameLbl;
	@property (nonatomic, assign) UILabel * dueLbl;
	@property (nonatomic, assign) UIView * pgrIndicatorView;
	@property (nonatomic, assign) UIProgressView * pgrView;
	@property (nonatomic, assign) UILabel * recentLap;
	@property (nonatomic, assign) UILabel * recentLap1;
	@property (nonatomic, assign) UILabel * recentLap2;
	@property (nonatomic, assign) UILabel * recentLap3;
	@property (nonatomic, assign) UILabel * recentLap4;
	@property (nonatomic, assign) NSLayoutConstraint * widthOfPgrView;
	-(void) release;
	-(id) retain;
	-(GCHandle) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (GCHandle) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(UILabel *) bestLap;
	-(void) setBestLap:(UILabel *)p0;
	-(UIImageView *) carImage;
	-(void) setCarImage:(UIImageView *)p0;
	-(UILabel *) carName;
	-(void) setCarName:(UILabel *)p0;
	-(UIView *) carnameVw;
	-(void) setCarnameVw:(UIView *)p0;
	-(UILabel *) cNameLbl;
	-(void) setCNameLbl:(UILabel *)p0;
	-(UILabel *) dueLbl;
	-(void) setDueLbl:(UILabel *)p0;
	-(UIView *) pgrIndicatorView;
	-(void) setPgrIndicatorView:(UIView *)p0;
	-(UIProgressView *) pgrView;
	-(void) setPgrView:(UIProgressView *)p0;
	-(UILabel *) recentLap;
	-(void) setRecentLap:(UILabel *)p0;
	-(UILabel *) recentLap1;
	-(void) setRecentLap1:(UILabel *)p0;
	-(UILabel *) recentLap2;
	-(void) setRecentLap2:(UILabel *)p0;
	-(UILabel *) recentLap3;
	-(void) setRecentLap3:(UILabel *)p0;
	-(UILabel *) recentLap4;
	-(void) setRecentLap4:(UILabel *)p0;
	-(NSLayoutConstraint *) widthOfPgrView;
	-(void) setWidthOfPgrView:(NSLayoutConstraint *)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface UITableViewSource : NSObject<UIScrollViewDelegate> {
}
	-(id) init;
@end

@interface VBOXStopwatch_CarsListViewDataSource : NSObject<UIScrollViewDelegate> {
}
	-(void) release;
	-(id) retain;
	-(GCHandle) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (GCHandle) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(UITableViewCell *) tableView:(UITableView *)p0 cellForRowAtIndexPath:(NSIndexPath *)p1;
	-(NSInteger) tableView:(UITableView *)p0 numberOfRowsInSection:(NSInteger)p1;
	-(CGFloat) tableView:(UITableView *)p0 heightForRowAtIndexPath:(NSIndexPath *)p1;
	-(BOOL) tableView:(UITableView *)p0 canEditRowAtIndexPath:(NSIndexPath *)p1;
	-(void) tableView:(UITableView *)p0 commitEditingStyle:(NSInteger)p1 forRowAtIndexPath:(NSIndexPath *)p2;
	-(void) tableView:(UITableView *)p0 didSelectRowAtIndexPath:(NSIndexPath *)p1;
	-(NSString *) tableView:(UITableView *)p0 titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)p1;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface FullLapsList : UIViewController {
}
	@property (nonatomic, assign) UITableView * lapsInfoTblView;
	@property (nonatomic, assign) UILabel * titleLabel;
	-(void) release;
	-(id) retain;
	-(GCHandle) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (GCHandle) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(UITableView *) lapsInfoTblView;
	-(void) setLapsInfoTblView:(UITableView *)p0;
	-(UILabel *) titleLabel;
	-(void) setTitleLabel:(UILabel *)p0;
	-(void) viewDidLoad;
	-(void) didReceiveMemoryWarning;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface VBOXStopwatch_UserControlls_FullLapsListDataSource : NSObject<UIScrollViewDelegate> {
}
	-(void) release;
	-(id) retain;
	-(GCHandle) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (GCHandle) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(UITableViewCell *) tableView:(UITableView *)p0 cellForRowAtIndexPath:(NSIndexPath *)p1;
	-(NSInteger) tableView:(UITableView *)p0 numberOfRowsInSection:(NSInteger)p1;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface LapDetailsViewController : UIViewController {
}
	@property (nonatomic, assign) UILabel * carNameLable;
	@property (nonatomic, assign) UITableView * lapsListView;
	-(void) release;
	-(id) retain;
	-(GCHandle) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (GCHandle) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(UILabel *) carNameLable;
	-(void) setCarNameLable:(UILabel *)p0;
	-(UITableView *) lapsListView;
	-(void) setLapsListView:(UITableView *)p0;
	-(void) viewDidLoad;
	-(void) didReceiveMemoryWarning;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface ViewController : UIViewController<UIScrollViewDelegate, UIDragInteractionDelegate, UIDropInteractionDelegate, UITableViewDropDelegate> {
}
	@property (nonatomic, assign) UIView * bottomView;
	@property (nonatomic, assign) UITableView * CarsListView;
	@property (nonatomic, assign) UIButton * deleteBtn;
	@property (nonatomic, assign) UIScrollView * ImagesScrlView;
	@property (nonatomic, assign) UIButton * lapButtonClicked;
	@property (nonatomic, assign) UIView * livePreviewView;
	@property (nonatomic, assign) UIButton * oneMoreLapsButton;
	@property (nonatomic, assign) UIButton * refreshButton;
	@property (nonatomic, assign) UIButton * settingsButton;
	@property (nonatomic, assign) UIButton * showCamButton;
	@property (nonatomic, assign) UIView * showHideView;
	@property (nonatomic, assign) UIButton * stopButtonClicked;
	@property (nonatomic, assign) UIView * topView;
	@property (nonatomic, assign) UIButton * undoRedoButton;
	-(void) release;
	-(id) retain;
	-(GCHandle) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (GCHandle) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(UIView *) bottomView;
	-(void) setBottomView:(UIView *)p0;
	-(UITableView *) CarsListView;
	-(void) setCarsListView:(UITableView *)p0;
	-(UIButton *) deleteBtn;
	-(void) setDeleteBtn:(UIButton *)p0;
	-(UIScrollView *) ImagesScrlView;
	-(void) setImagesScrlView:(UIScrollView *)p0;
	-(UIButton *) lapButtonClicked;
	-(void) setLapButtonClicked:(UIButton *)p0;
	-(UIView *) livePreviewView;
	-(void) setLivePreviewView:(UIView *)p0;
	-(UIButton *) oneMoreLapsButton;
	-(void) setOneMoreLapsButton:(UIButton *)p0;
	-(UIButton *) refreshButton;
	-(void) setRefreshButton:(UIButton *)p0;
	-(UIButton *) settingsButton;
	-(void) setSettingsButton:(UIButton *)p0;
	-(UIButton *) showCamButton;
	-(void) setShowCamButton:(UIButton *)p0;
	-(UIView *) showHideView;
	-(void) setShowHideView:(UIView *)p0;
	-(UIButton *) stopButtonClicked;
	-(void) setStopButtonClicked:(UIButton *)p0;
	-(UIView *) topView;
	-(void) setTopView:(UIView *)p0;
	-(UIButton *) undoRedoButton;
	-(void) setUndoRedoButton:(UIButton *)p0;
	-(void) viewDidLoad;
	-(void) viewDidAppear:(BOOL)p0;
	-(void) didReceiveMemoryWarning;
	-(BOOL) dropInteraction:(UIDropInteraction *)p0 canHandleSession:(id)p1;
	-(void) dropInteraction:(UIDropInteraction *)p0 sessionDidEnter:(id)p1;
	-(UIDropProposal *) dropInteraction:(UIDropInteraction *)p0 sessionDidUpdate:(id)p1;
	-(void) dropInteraction:(UIDropInteraction *)p0 sessionDidExit:(id)p1;
	-(void) dropInteraction:(UIDropInteraction *)p0 sessionDidEnd:(id)p1;
	-(void) tableView:(UITableView *)p0 performDropWithCoordinator:(id)p1;
	-(NSArray *) dragInteraction:(UIDragInteraction *)p0 itemsForBeginningSession:(id)p1;
	-(UITargetedDragPreview *) dragInteraction:(UIDragInteraction *)p0 previewForLiftingItem:(UIDragItem *)p1 session:(id)p2;
	-(void) deleteAllButton:(NSObject *)p0;
	-(void) LapButtonClicked_TouchUpInside:(UIButton *)p0;
	-(void) OneMoreLapsButtonAction:(NSObject *)p0;
	-(void) refreshButtonClicked:(NSObject *)p0;
	-(void) SettingsButton_TouchUpInside:(UIButton *)p0;
	-(void) showHideCameraPreview:(NSObject *)p0;
	-(void) stopButtonAction:(NSObject *)p0;
	-(void) UndoRedoButton_TouchUpInside:(UIButton *)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface SettingsViewController : UIViewController {
}
	@property (nonatomic, assign) UILabel * appVersionNumber;
	@property (nonatomic, assign) UIButton * backBtn;
	@property (nonatomic, assign) UIButton * closeBtn;
	@property (nonatomic, assign) UIButton * deleteAllBtn;
	@property (nonatomic, assign) UIButton * newSessionBtn;
	@property (nonatomic, assign) UISwitch * previewSwitch;
	@property (nonatomic, assign) UIButton * supportBtn;
	-(void) release;
	-(id) retain;
	-(GCHandle) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (GCHandle) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(UILabel *) appVersionNumber;
	-(void) setAppVersionNumber:(UILabel *)p0;
	-(UIButton *) backBtn;
	-(void) setBackBtn:(UIButton *)p0;
	-(UIButton *) closeBtn;
	-(void) setCloseBtn:(UIButton *)p0;
	-(UIButton *) deleteAllBtn;
	-(void) setDeleteAllBtn:(UIButton *)p0;
	-(UIButton *) newSessionBtn;
	-(void) setNewSessionBtn:(UIButton *)p0;
	-(UISwitch *) previewSwitch;
	-(void) setPreviewSwitch:(UISwitch *)p0;
	-(UIButton *) supportBtn;
	-(void) setSupportBtn:(UIButton *)p0;
	-(void) viewDidLoad;
	-(void) BackBtn_TouchUpInside:(UIButton *)p0;
	-(void) CameraPreview:(NSObject *)p0;
	-(void) closeBtnCliked:(NSObject *)p0;
	-(void) deleteAllBtnClicked:(NSObject *)p0;
	-(void) newSessionClicked:(NSObject *)p0;
	-(void) suppportBtnClicked:(NSObject *)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface CustomCameraViewController : UIViewController {
}
	@property (nonatomic, assign) UIButton * backBtn;
	-(void) release;
	-(id) retain;
	-(GCHandle) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (GCHandle) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(UIButton *) backBtn;
	-(void) setBackBtn:(UIButton *)p0;
	-(void) viewDidLoad;
	-(void) didReceiveMemoryWarning;
	-(void) BackBtn_TouchUpInside:(UIButton *)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface __UIGestureRecognizerToken : NSObject {
}
	-(void) release;
	-(id) retain;
	-(GCHandle) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (GCHandle) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface UIKit_UIView_UIViewAppearance : NSObject {
}
	-(void) release;
	-(id) retain;
	-(GCHandle) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (GCHandle) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface UIKit_UIScrollView_UIScrollViewAppearance : UIKit_UIView_UIViewAppearance {
}
@end

@interface TOActivityCroppedImageProvider : UIActivityItemProvider {
}
	-(NSInteger) angle;
	-(BOOL) circular;
	-(CGRect) cropFrame;
	-(UIImage *) image;
	-(id) init;
	-(id) initWithImage:(UIImage *)p0 cropFrame:(CGRect)p1 angle:(NSInteger)p2 circular:(BOOL)p3;
@end

@interface TOCroppedImageAttributes : NSObject {
}
	-(NSInteger) angle;
	-(CGRect) croppedFrame;
	-(CGSize) originalImageSize;
	-(id) init;
	-(id) initWithCroppedFrame:(CGRect)p0 angle:(NSInteger)p1 originalImageSize:(CGSize)p2;
@end

@interface TOCropViewController : UIViewController {
}
	-(void) dismissAnimatedFromParentViewController:(UIViewController *)p0 toView:(UIView *)p1 toFrame:(CGRect)p2 setup:(void (^)())p3 completion:(void (^)())p4;
	-(void) dismissAnimatedFromParentViewController:(UIViewController *)p0 withCroppedImage:(UIImage *)p1 toView:(UIView *)p2 toFrame:(CGRect)p3 setup:(void (^)())p4 completion:(void (^)())p5;
	-(void) presentAnimatedFromParentViewController:(UIViewController *)p0 fromView:(UIView *)p1 fromFrame:(CGRect)p2 setup:(void (^)())p3 completion:(void (^)())p4;
	-(void) presentAnimatedFromParentViewController:(UIViewController *)p0 fromImage:(UIImage *)p1 fromView:(UIView *)p2 fromFrame:(CGRect)p3 angle:(NSInteger)p4 toImageFrame:(CGRect)p5 setup:(void (^)())p6 completion:(void (^)())p7;
	-(void) resetCropViewLayout;
	-(void) setAspectRatioPreset:(NSInteger)p0 animated:(BOOL)p1;
	-(NSArray *) activityItems;
	-(void) setActivityItems:(NSArray *)p0;
	-(NSArray *) allowedAspectRatios;
	-(void) setAllowedAspectRatios:(NSArray *)p0;
	-(NSInteger) angle;
	-(void) setAngle:(NSInteger)p0;
	-(NSArray *) applicationActivities;
	-(void) setApplicationActivities:(NSArray *)p0;
	-(BOOL) aspectRatioLockDimensionSwapEnabled;
	-(void) setAspectRatioLockDimensionSwapEnabled:(BOOL)p0;
	-(BOOL) aspectRatioLockEnabled;
	-(void) setAspectRatioLockEnabled:(BOOL)p0;
	-(BOOL) aspectRatioPickerButtonHidden;
	-(void) setAspectRatioPickerButtonHidden:(BOOL)p0;
	-(NSInteger) aspectRatioPreset;
	-(void) setAspectRatioPreset:(NSInteger)p0;
	-(NSString *) cancelButtonTitle;
	-(void) setCancelButtonTitle:(NSString *)p0;
	-(id) cropView;
	-(NSInteger) croppingStyle;
	-(CGSize) customAspectRatio;
	-(void) setCustomAspectRatio:(CGSize)p0;
	-(NSString *) customAspectRatioName;
	-(void) setCustomAspectRatioName:(NSString *)p0;
	-(NSString *) doneButtonTitle;
	-(void) setDoneButtonTitle:(NSString *)p0;
	-(NSArray *) excludedActivityTypes;
	-(void) setExcludedActivityTypes:(NSArray *)p0;
	-(BOOL) hidesNavigationBar;
	-(void) setHidesNavigationBar:(BOOL)p0;
	-(UIImage *) image;
	-(CGRect) imageCropFrame;
	-(void) setImageCropFrame:(CGRect)p0;
	-(CGFloat) minimumAspectRatio;
	-(void) setMinimumAspectRatio:(CGFloat)p0;
	-(id) onDidCropImageToRect;
	-(void) setOnDidCropImageToRect:(void (^)(void *, void *))p0;
	-(id) onDidCropToCircleImage;
	-(void) setOnDidCropToCircleImage:(void (^)(void *, void *, void *))p0;
	-(id) onDidCropToRect;
	-(void) setOnDidCropToRect:(void (^)(void *, void *, void *))p0;
	-(id) onDidFinishCancelled;
	-(void) setOnDidFinishCancelled:(void (^)(void *))p0;
	-(BOOL) resetAspectRatioEnabled;
	-(void) setResetAspectRatioEnabled:(BOOL)p0;
	-(BOOL) resetButtonHidden;
	-(void) setResetButtonHidden:(BOOL)p0;
	-(BOOL) rotateButtonsHidden;
	-(void) setRotateButtonsHidden:(BOOL)p0;
	-(BOOL) rotateClockwiseButtonHidden;
	-(void) setRotateClockwiseButtonHidden:(BOOL)p0;
	-(BOOL) showActivitySheetOnDone;
	-(void) setShowActivitySheetOnDone:(BOOL)p0;
	-(BOOL) showCancelConfirmationDialog;
	-(void) setShowCancelConfirmationDialog:(BOOL)p0;
	-(UILabel *) titleLabel;
	-(id) toolbar;
	-(NSInteger) toolbarPosition;
	-(void) setToolbarPosition:(NSInteger)p0;
	-(NSObject *) delegate;
	-(void) setDelegate:(NSObject *)p0;
	-(id) init;
	-(id) initWithCoder:(NSCoder *)p0;
	-(id) initWithImage:(UIImage *)p0;
	-(id) initWithCroppingStyle:(NSInteger)p0 image:(UIImage *)p1;
@end

@interface TOCropViewControllerTransitioning : NSObject {
}
	-(void) animateTransition:(id)p0;
	-(void) reset;
	-(double) transitionDuration:(id)p0;
	-(CGRect) fromFrame;
	-(void) setFromFrame:(CGRect)p0;
	-(UIView *) fromView;
	-(void) setFromView:(UIView *)p0;
	-(UIImage *) image;
	-(void) setImage:(UIImage *)p0;
	-(BOOL) isDismissing;
	-(void) setIsDismissing:(BOOL)p0;
	-(id) prepareForTransitionHandler;
	-(void) setPrepareForTransitionHandler:(void (^)())p0;
	-(CGRect) toFrame;
	-(void) setToFrame:(CGRect)p0;
	-(UIView *) toView;
	-(void) setToView:(UIView *)p0;
	-(id) init;
@end

@protocol TOCropViewDelegate
	@required -(void) cropViewDidBecomeResettable:(id)p0;
	@required -(void) cropViewDidBecomeNonResettable:(id)p0;
@end

@interface TOCropViewDelegate : NSObject<TOCropViewDelegate> {
}
	-(id) init;
@end

@interface Xamarin_TOCropView_TOCropOverlayView_TOCropOverlayViewAppearance : UIKit_UIView_UIViewAppearance {
}
@end

@interface TOCropOverlayView : UIView {
}
	-(void) setGridHidden:(BOOL)p0 animated:(BOOL)p1;
	-(BOOL) displayHorizontalGridLines;
	-(void) setDisplayHorizontalGridLines:(BOOL)p0;
	-(BOOL) displayVerticalGridLines;
	-(void) setDisplayVerticalGridLines:(BOOL)p0;
	-(BOOL) gridHidden;
	-(void) setGridHidden:(BOOL)p0;
	-(id) init;
	-(id) initWithCoder:(NSCoder *)p0;
@end

@interface Xamarin_TOCropView_TOCropScrollView_TOCropScrollViewAppearance : UIKit_UIScrollView_UIScrollViewAppearance {
}
@end

@interface TOCropScrollView : UIScrollView {
}
	-(id) touchesBegan;
	-(void) setTouchesBegan:(void (^)())p0;
	-(id) touchesCancelled;
	-(void) setTouchesCancelled:(void (^)())p0;
	-(id) touchesEnded;
	-(void) setTouchesEnded:(void (^)())p0;
	-(id) init;
	-(id) initWithCoder:(NSCoder *)p0;
@end

@interface Xamarin_TOCropView_TOCropToolbar_TOCropToolbarAppearance : UIKit_UIView_UIViewAppearance {
}
@end

@interface TOCropToolbar : UIView {
}
	-(UIEdgeInsets) backgroundViewOutsets;
	-(void) setBackgroundViewOutsets:(UIEdgeInsets)p0;
	-(id) cancelButtonTapped;
	-(void) setCancelButtonTapped:(void (^)())p0;
	-(UIButton *) cancelIconButton;
	-(UIButton *) cancelTextButton;
	-(NSString *) cancelTextButtonTitle;
	-(void) setCancelTextButtonTitle:(NSString *)p0;
	-(UIButton *) clampButton;
	-(CGRect) clampButtonFrame;
	-(BOOL) clampButtonGlowing;
	-(void) setClampButtonGlowing:(BOOL)p0;
	-(BOOL) clampButtonHidden;
	-(void) setClampButtonHidden:(BOOL)p0;
	-(id) clampButtonTapped;
	-(void) setClampButtonTapped:(void (^)())p0;
	-(CGRect) doneButtonFrame;
	-(id) doneButtonTapped;
	-(void) setDoneButtonTapped:(void (^)())p0;
	-(UIButton *) doneIconButton;
	-(UIButton *) doneTextButton;
	-(NSString *) doneTextButtonTitle;
	-(void) setDoneTextButtonTitle:(NSString *)p0;
	-(UIButton *) resetButton;
	-(BOOL) resetButtonEnabled;
	-(void) setResetButtonEnabled:(BOOL)p0;
	-(BOOL) resetButtonHidden;
	-(void) setResetButtonHidden:(BOOL)p0;
	-(id) resetButtonTapped;
	-(void) setResetButtonTapped:(void (^)())p0;
	-(UIButton *) rotateButton;
	-(UIButton *) rotateClockwiseButton;
	-(BOOL) rotateClockwiseButtonHidden;
	-(void) setRotateClockwiseButtonHidden:(BOOL)p0;
	-(id) rotateClockwiseButtonTapped;
	-(void) setRotateClockwiseButtonTapped:(void (^)())p0;
	-(UIButton *) rotateCounterclockwiseButton;
	-(BOOL) rotateCounterclockwiseButtonHidden;
	-(void) setRotateCounterclockwiseButtonHidden:(BOOL)p0;
	-(id) rotateCounterclockwiseButtonTapped;
	-(void) setRotateCounterclockwiseButtonTapped:(void (^)())p0;
	-(CGFloat) statusBarHeightInset;
	-(void) setStatusBarHeightInset:(CGFloat)p0;
	-(id) init;
	-(id) initWithCoder:(NSCoder *)p0;
@end

@interface Xamarin_TOCropView_TOCropView_TOCropViewAppearance : UIKit_UIView_UIViewAppearance {
}
@end

@interface TOCropView : UIView {
}
	-(void) moveCroppedContentToCenterAnimated:(BOOL)p0;
	-(void) performInitialSetup;
	-(void) performRelayoutForRotation;
	-(void) prepareforRotation;
	-(void) resetLayoutToDefaultAnimated:(BOOL)p0;
	-(void) rotateImageNinetyDegreesAnimated:(BOOL)p0;
	-(void) rotateImageNinetyDegreesAnimated:(BOOL)p0 clockwise:(BOOL)p1;
	-(void) setAspectRatio:(CGSize)p0 animated:(BOOL)p1;
	-(void) setBackgroundImageViewHidden:(BOOL)p0 animated:(BOOL)p1;
	-(void) setCroppingViewsHidden:(BOOL)p0 animated:(BOOL)p1;
	-(void) setGridOverlayHidden:(BOOL)p0 animated:(BOOL)p1;
	-(void) setSimpleRenderMode:(BOOL)p0 animated:(BOOL)p1;
	-(BOOL) alwaysShowCroppingGrid;
	-(void) setAlwaysShowCroppingGrid:(BOOL)p0;
	-(NSInteger) angle;
	-(void) setAngle:(NSInteger)p0;
	-(CGSize) aspectRatio;
	-(void) setAspectRatio:(CGSize)p0;
	-(BOOL) aspectRatioLockDimensionSwapEnabled;
	-(void) setAspectRatioLockDimensionSwapEnabled:(BOOL)p0;
	-(BOOL) aspectRatioLockEnabled;
	-(void) setAspectRatioLockEnabled:(BOOL)p0;
	-(BOOL) canBeReset;
	-(double) cropAdjustingDelay;
	-(void) setCropAdjustingDelay:(double)p0;
	-(BOOL) cropBoxAspectRatioIsPortrait;
	-(CGRect) cropBoxFrame;
	-(BOOL) cropBoxResizeEnabled;
	-(void) setCropBoxResizeEnabled:(BOOL)p0;
	-(UIEdgeInsets) cropRegionInsets;
	-(void) setCropRegionInsets:(UIEdgeInsets)p0;
	-(CGFloat) cropViewPadding;
	-(void) setCropViewPadding:(CGFloat)p0;
	-(NSInteger) croppingStyle;
	-(BOOL) croppingViewsHidden;
	-(void) setCroppingViewsHidden:(BOOL)p0;
	-(UIView *) foregroundContainerView;
	-(BOOL) gridOverlayHidden;
	-(void) setGridOverlayHidden:(BOOL)p0;
	-(id) gridOverlayView;
	-(UIImage *) image;
	-(CGRect) imageCropFrame;
	-(void) setImageCropFrame:(CGRect)p0;
	-(CGRect) imageViewFrame;
	-(BOOL) internalLayoutDisabled;
	-(void) setInternalLayoutDisabled:(BOOL)p0;
	-(CGFloat) maximumZoomScale;
	-(void) setMaximumZoomScale:(CGFloat)p0;
	-(CGFloat) minimumAspectRatio;
	-(void) setMinimumAspectRatio:(CGFloat)p0;
	-(BOOL) resetAspectRatioEnabled;
	-(void) setResetAspectRatioEnabled:(BOOL)p0;
	-(BOOL) simpleRenderMode;
	-(void) setSimpleRenderMode:(BOOL)p0;
	-(BOOL) translucencyAlwaysHidden;
	-(void) setTranslucencyAlwaysHidden:(BOOL)p0;
	-(NSObject *) delegate;
	-(void) setDelegate:(NSObject *)p0;
	-(id) init;
	-(id) initWithCoder:(NSCoder *)p0;
	-(id) initWithImage:(UIImage *)p0;
	-(id) initWithCroppingStyle:(NSInteger)p0 image:(UIImage *)p1;
@end


