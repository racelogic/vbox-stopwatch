﻿using System;
using System.Collections.Generic;
using Foundation;
using UIKit;

namespace VBOXStopwatch.UserControlls
{
    public class FullLapsListDataSource: UITableViewSource
    {
        private List<string> data;

        public FullLapsListDataSource()
        {
        }

        public FullLapsListDataSource(List<string> data)
        {
            this.data = data;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = (UITableViewCell)tableView.DequeueReusableCell("lapInfo", indexPath);
           
            var d = data[indexPath.Row];
            var firstAttribute = new UIStringAttributes
            {
                ForegroundColor = UIColor.Gray,
            };

            var secondAttribute = new UIStringAttributes
            {
                ForegroundColor = UIColor.FromRGB(56, 158, 42),
                Font = UIFont.FromName("Hiragino Mincho ProN W6", 18), //UIFont.BoldSystemFontOfSize(18.0f),
            };

            var fontAttribute = new UIStringAttributes
            {
                Font = UIFont.FromName("Hiragino Mincho ProN", 18),//UIFont.SystemFontOfSize(18.0f),
            };

            var thirdAttribute = new UIStringAttributes
            {
                ForegroundColor = UIColor.Black,
            };

            var fourthAttribute = new UIStringAttributes
            {
                ForegroundColor = UIColor.FromRGB(47, 1, 54),
            };

            var recentLapPrettyString = new NSMutableAttributedString(d);
            if (d.Contains("+0.00"))
            {
                recentLapPrettyString.SetAttributes(secondAttribute, new NSRange(0, (d.Length)));
            }
            else
            {
                recentLapPrettyString.SetAttributes(fontAttribute, new NSRange(0, (d.Length)));
            }
            //else
            //{
            //    recentLapPrettyString.SetAttributes(firstAttribute, new NSRange(0, 17));
            //    recentLapPrettyString.SetAttributes(thirdAttribute, new NSRange(17, 25));
            //}

            cell.TextLabel.AttributedText = recentLapPrettyString;
            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return data.Count;
        }
    }
}
