using Foundation;
using System;
using UIKit;
using System.Collections.Generic;
using System.IO;
using CoreGraphics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Threading.Tasks;

namespace VBOXStopwatch
{
    public partial class CarsCustomCell : UITableViewCell
    {
        //Dictionary<string, CarsInfo> selectedData = new NSDictionary<string, CarsInfo>();
        CarsInfo selectedCar = new CarsInfo();
        public CarsCustomCell (IntPtr handle) : base (handle)
        {
           
        }

        internal void UpdateCell(CarsInfo data)
        {
            var firstAttribute = new UIStringAttributes
            {
                ForegroundColor = UIColor.Gray,
            };

            var secondAttribute = new UIStringAttributes
            {
                ForegroundColor = UIColor.FromRGB(56,158,42),
            };

            var thirdAttribute = new UIStringAttributes
            {
                ForegroundColor = UIColor.Black,
            };

            var fourthAttribute = new UIStringAttributes
            {
                ForegroundColor = UIColor.FromRGB(47, 1, 54),
            };

            cNameLbl.AdjustsFontSizeToFitWidth = true;
            cNameLbl.MinimumScaleFactor = 0.2f;
            cNameLbl.Lines = 0;

            //carName.TextAlignment = UITextAlignment.Right;
            carnameVw.BackgroundColor = UIColor.Clear;
            BringSubviewToFront(carnameVw);
            cNameLbl.Text = data.CarName;
            cNameLbl.UserInteractionEnabled = true;



            //selectedData[data.CarName] = data;
            selectedCar = data;

            //byte[] imageArray // is your data
            //MemoryStream mStream = new MemoryStream();
            //mStream.Write(data.CarImage, 0, data.CarImage.Length); //write(imageArray, 0, imageArray.Length);
            //Image img = Image.FromStream(mStream);

            //byte[] img = Tools.HexStringToBytes(vc);
            //var data = NSData.FromArray(img);
            //var uiimage = UIImage.LoadFromData(data);

            using (var imgdata = NSData.FromArray(data.CarImage))
            {
                var img = UIImage.LoadFromData(imgdata);
                carImage.Image = img;
                //carImage.Transform = CGAffineTransform.MakeRotation(90);
            } 

            //carImage.Image = UIImage.LoadFromData(NSData.FromArray(data.CarImage));
            carImage.ContentMode = UIViewContentMode.ScaleAspectFit;
            carImage.UserInteractionEnabled = true;
            UITapGestureRecognizer imageTap = new UITapGestureRecognizer(TapImage);
            imageTap.NumberOfTapsRequired = 1;
            carImage.AddGestureRecognizer(imageTap);
            carImage.ContentMode = UIViewContentMode.ScaleAspectFill; //.scaleToFill

            var whiteSpaces = "";
            if (carName.Text != "")
            {
                var carnameLength = carName.Text.Length;
                var getSpacesCount = 12 - carnameLength;
                for(int i = 0; i < getSpacesCount; i++)
                {
                    whiteSpaces += " "; 
                }
            }

            //carName.Text = data.CarName;
            //carName.Text = data.timerValue; //quick check about how timer works

            /*
                        Can we instead give a ‘count down’ as we near the end of the lap? So it would work like follows:

                        When the current lap time is within 15s of the lap being due, a countdown starts.

                        When the countdown reaches 10s, the background goes orange.

                        When the countdown reaches 5s we change the background to red.

                        When the countdown reaches 0s, we see ‘Lap due’

                        The countdown is the difference between elapsed lap time and the best laptime.
            */
            if (data.timerValue != "")
            {
                int timerValue = Convert.ToInt32(data.timerValue);
                if (timerValue <= 1)
                {
                    //carName.Text = data.CarName + whiteSpaces + " : Due";  //"Due" + ": " + data.CarName + " ";
                    dueLbl.Text = ": Due";
                    pgrView.TintColor = UIColor.FromRGB(255, 0, 0);//Red
                }
                else if (timerValue <= 5)
                {
                    //carName.Text = data.CarName + whiteSpaces + " : " + data.timerValue + " ";
                    dueLbl.Text = " " + data.timerValue + " ";
                    pgrView.TintColor = UIColor.FromRGB(253, 0, 0);//Red
                }
                else if (timerValue <= 9)
                {
                    //carName.Text = data.CarName + whiteSpaces + " : " + data.timerValue + " "; // carName.Text = data.CarName + ": " + whiteSpaces + data.timerValue + " ";
                    dueLbl.Text = " " + data.timerValue + " ";
                    pgrView.TintColor = UIColor.FromRGB(253, 134, 39);//Orange
                }
                else if (timerValue <= 15)
                {
                    //carName.Text = data.CarName + whiteSpaces + " : " + data.timerValue + " ";
                    pgrView.TintColor = UIColor.FromRGB(255, 255, 255);
                    dueLbl.Text = " " + data.timerValue + " ";
                } else
                {
                    //carName.Text = data.CarName;
                    pgrView.TintColor = UIColor.FromRGB(255, 255, 255);
                    dueLbl.Text = "";
                }
                //carName.UserInteractionEnabled = false;
            }
            else
            {
                //carName.UserInteractionEnabled = true;
                //carName.Text = data.CarName;
                pgrView.TintColor = UIColor.FromRGB(255, 255, 255);
                dueLbl.Text = "";
            }

            //carName.BackgroundColor = data.CarColor;
            /*
            if (data.lapColor == "Yellow")
            {
                carName.BackgroundColor = UIColor.Yellow;
            }
            else if (data.lapColor == "Orange")
            {
                carName.BackgroundColor = UIColor.Orange;
            }
            else if (data.lapColor == "Gray")
            {
                carName.BackgroundColor = UIColor.Gray;
            }
            else if (data.lapColor == "Blue")
            {
                carName.BackgroundColor = UIColor.Blue;
            }
            else if (data.lapColor == "LightGray")
            {
                carName.BackgroundColor = UIColor.LightGray;
            }
            else if (data.lapColor == "Green")
            {
                carName.BackgroundColor = UIColor.Green;
            }
            else if (data.lapColor == "Magenta")
            {
                carName.BackgroundColor = UIColor.Magenta;
            }
            else if (data.lapColor == "Red")
            {
                carName.BackgroundColor = UIColor.Red;
            }
            */

            //UIProgressView pv = new UIProgressView();
            //pgrView.Frame = new CGRect(145, 1, 120, 29);


            UITapGestureRecognizer tapGesture = new UITapGestureRecognizer(TapCar);
            tapGesture.NumberOfTapsRequired = 1;
            cNameLbl.AddGestureRecognizer(tapGesture);

            //if (bestLap.Text != "")
            //{
                carName.TextAlignment = UITextAlignment.Left;
            //}

            recentLap.Text = data.RecentLapTime;
            var recentLapPrettyString = new NSMutableAttributedString(recentLap.Text);
            if(recentLap.Text.Contains("LAP"))
            {
                recentLapPrettyString.SetAttributes(firstAttribute, new NSRange(0, 6));
                recentLapPrettyString.SetAttributes(thirdAttribute, new NSRange(6, (data.RecentLapTime.Length - 6)));
            }
            recentLap.AttributedText = recentLapPrettyString;


            //bestLap.AdjustsFontSizeToFitWidth = true;
            //bestLap.MinimumScaleFactor = 0.6f;
            //bestLap.Lines = 0;

            bestLap.Text = data.BestLapTime; 
            var bestPrettyString = new NSMutableAttributedString(bestLap.Text);
            bestPrettyString.SetAttributes(firstAttribute, new NSRange(0, 6));
            var len = data.BestLapTime.Length;
            var endIndex = len - 6;
            if (data.BestLapTime.Length > 8)
            {
                bestPrettyString.SetAttributes(secondAttribute, new NSRange(6, endIndex));
            }
            bestLap.AttributedText = bestPrettyString;

            //Progress bar
            pgrView.Layer.BorderColor = UIColor.Gray.CGColor;
            pgrView.Layer.BorderWidth = 0.3f;
            pgrView.SetProgress(1.0f, false);
/*
            if (data.duePercent >= 0.85)
            {
                //pgrView.ProgressTintColor = UIColor.FromRGB(253, 134, 39);//UIColor.Orange;
                pgrView.TintColor = UIColor.FromRGB(253, 134, 39);
            }
            else
            {
                //pgrView.ProgressTintColor = UIColor.FromRGB(137, 191, 255);
                pgrView.TintColor = UIColor.FromRGB(255, 255, 255);
            }
*/
            /*
            var barPercent = 1.0f - data.duePercent;
            if(barPercent > 0.0f)
            {
                pgrIndicatorView.BackgroundColor = UIColor.Red;
                Console.WriteLine("000000000000000: " + barPercent);
                widthOfPgrView.Constant = pgrIndicatorView.Frame.Width * (-barPercent);//(-0.1f);//data.duePercent; //0.1=90, 0.2=80, 0.3= 70, 0.4=60, 0.5=50, 0.6=40, 0.7=30, 0.8=20, 0.9=10, 1.0=0 then (1.0 - data.duePercent)
            }
            */
            //pgrView.SetProgress(data.duePercent, false);
            
            //Console.WriteLine("Bar Latest: " + pgrView.Progress + " Actual Value: " + data.duePercent + " Car Name: "+ data.CarName);
            //------------

            recentLap1.Text = data.Lap1 == null ? "" : data.Lap1;

           
            if(data.Lap2 != "")
            {
                var lap1AttrStr = new NSMutableAttributedString(data.Lap1);
                var lap1Len = lap1AttrStr.Length;
                if (lap1Len > 0)
                {
                    var compare1 = data.Lap1.Split(' ')[2];
                    if (compare1.Contains("0.0"))
                    {
                        lap1AttrStr.SetAttributes(secondAttribute, new NSRange(lap1Len - 5, 5));
                    }
                    recentLap1.AttributedText = lap1AttrStr;
                }
            }
            
            recentLap2.Text = data.Lap2 == null ? "" : data.Lap2;

            var lap2AttrStr = new NSMutableAttributedString(recentLap2.Text);
            var lap2Len = lap2AttrStr.Length;
            if (lap2Len > 0)
            {
                var compare2 = data.Lap2.Split(' ')[2];
                if (compare2.Contains("0.0"))
                {
                    lap2AttrStr.SetAttributes(secondAttribute, new NSRange(lap2Len - 5, 5));
                }
                recentLap2.AttributedText = lap2AttrStr;
            }

            recentLap3.Text = data.Lap3 == null ? "" : data.Lap3;

            var lap3AttrStr = new NSMutableAttributedString(recentLap3.Text);
            var lap3Len = lap3AttrStr.Length;
            if (lap3Len > 0)
            {
                var compare3 = data.Lap3.Split(' ')[2];
                if (compare3.Contains("0.0"))
                {
                    lap3AttrStr.SetAttributes(secondAttribute, new NSRange(lap3Len - 5, 5));
                }
                recentLap3.AttributedText = lap3AttrStr;
            }

            recentLap4.Text = data.Lap4 == null ? "" : data.Lap4;

            var lap4AttrStr = new NSMutableAttributedString(recentLap4.Text);
            var lap4Len = lap4AttrStr.Length;
            if (lap4Len > 0)
            {
                var compare4 = data.Lap4.Split(' ')[2];
                if (compare4.Contains("0.0"))
                {
                    lap4AttrStr.SetAttributes(secondAttribute, new NSRange(lap4Len - 5, 5));
                }
                recentLap4.AttributedText = lap4AttrStr;
            }
        }
        
        void TapCar(UITapGestureRecognizer tap1)
        {
            //lblDisplay.Text = "You Touched Me..";
            
            //Console.WriteLine(AppDelegate.carsInfoDataDelegate.Count);
            //Console.WriteLine("Lable clicked on cell");
            AppDelegate.selectedCarData["changeCarName"] = selectedCar;
            NSNotificationCenter.DefaultCenter.PostNotificationName("ChangeName", null);
        }

        void TapImage(UITapGestureRecognizer tap2)
        {
            //Console.WriteLine("Tapped on the Image");
            AppDelegate.selectedCarData["changeCarName"] = selectedCar;
            NSNotificationCenter.DefaultCenter.PostNotificationName("ChangeImage", null);
        }

        // bytes must be a valid image
        private UIImage ImageFromBytes(byte[] bytes, nfloat width, nfloat height)
        {
            try
            {
                NSData data = NSData.FromArray(bytes);
                UIImage image = UIImage.LoadFromData(data);
                CGSize scaleSize = new CGSize(width, height);
                UIGraphics.BeginImageContextWithOptions(scaleSize, false, 0);
                image.Draw(new CGRect(0, 0, scaleSize.Width, scaleSize.Height));
                UIImage resizedImage = UIGraphics.GetImageFromCurrentImageContext();
                UIGraphics.EndImageContext();
                return resizedImage;
            }
            catch (Exception)
            {
                return null;
            }
        }

        //Crops an image to even width and height
        public UIImage CenterCrop(UIImage originalImage)
        {
            // Use smallest side length as crop square length
            double squareLength = Math.Min(originalImage.Size.Width, originalImage.Size.Height);

            nfloat x, y;
            x = (nfloat)((originalImage.Size.Width - squareLength) / 2.0);
            y = (nfloat)((originalImage.Size.Height - squareLength) / 2.0);

            //This Rect defines the coordinates to be used for the crop
            CGRect croppedRect = CGRect.FromLTRB(x, y, x + (nfloat)squareLength, y + (nfloat)squareLength);

            // Center-Crop the image
            UIGraphics.BeginImageContextWithOptions(croppedRect.Size, false, originalImage.CurrentScale);
            originalImage.Draw(new CGPoint(-croppedRect.X, -croppedRect.Y));
            UIImage croppedImage = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();

            return croppedImage;
        }

        public static Image ResizeImage(Image imgToResize, Size destinationSize)
        {
            var originalWidth = imgToResize.Width;
            var originalHeight = imgToResize.Height;

            //how many units are there to make the original length
            var hRatio = (float)originalHeight / destinationSize.Height;
            var wRatio = (float)originalWidth / destinationSize.Width;

            //get the shorter side
            var ratio = Math.Min(hRatio, wRatio);

            var hScale = Convert.ToInt32(destinationSize.Height * ratio);
            var wScale = Convert.ToInt32(destinationSize.Width * ratio);

            //start cropping from the center
            var startX = (originalWidth - wScale) / 2;
            var startY = (originalHeight - hScale) / 2;

            //crop the image from the specified location and size
            var sourceRectangle = new Rectangle(startX, startY, wScale, hScale);

            //the future size of the image
            var bitmap = new Bitmap(destinationSize.Width, destinationSize.Height);

            //fill-in the whole bitmap
            var destinationRectangle = new Rectangle(0, 0, bitmap.Width, bitmap.Height);

            //generate the new image
            using (var g = Graphics.FromImage(bitmap))
            {
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(imgToResize, destinationRectangle, sourceRectangle, GraphicsUnit.Pixel);
            }

            return bitmap;

        }

    }
}