// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace VBOXStopwatch
{
	[Register ("CarsCustomCell")]
	partial class CarsCustomCell
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIKit.UILabel bestLap { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIKit.UIImageView carImage { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIKit.UILabel carName { get; set; }

		[Outlet]
		UIKit.UIView carnameVw { get; set; }

		[Outlet]
		UIKit.UILabel cNameLbl { get; set; }

		[Outlet]
		UIKit.UILabel dueLbl { get; set; }

		[Outlet]
		UIKit.UIView pgrIndicatorView { get; set; }

		[Outlet]
		public UIKit.UIProgressView pgrView { get; private set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		public UIKit.UILabel recentLap { get; private set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIKit.UILabel recentLap1 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIKit.UILabel recentLap2 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIKit.UILabel recentLap3 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIKit.UILabel recentLap4 { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint widthOfPgrView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (bestLap != null) {
				bestLap.Dispose ();
				bestLap = null;
			}

			if (carImage != null) {
				carImage.Dispose ();
				carImage = null;
			}

			if (carName != null) {
				carName.Dispose ();
				carName = null;
			}

			if (carnameVw != null) {
				carnameVw.Dispose ();
				carnameVw = null;
			}

			if (pgrIndicatorView != null) {
				pgrIndicatorView.Dispose ();
				pgrIndicatorView = null;
			}

			if (pgrView != null) {
				pgrView.Dispose ();
				pgrView = null;
			}

			if (recentLap != null) {
				recentLap.Dispose ();
				recentLap = null;
			}

			if (recentLap1 != null) {
				recentLap1.Dispose ();
				recentLap1 = null;
			}

			if (recentLap2 != null) {
				recentLap2.Dispose ();
				recentLap2 = null;
			}

			if (recentLap3 != null) {
				recentLap3.Dispose ();
				recentLap3 = null;
			}

			if (cNameLbl != null) {
				cNameLbl.Dispose ();
				cNameLbl = null;
			}

			if (dueLbl != null) {
				dueLbl.Dispose ();
				dueLbl = null;
			}

			if (recentLap4 != null) {
				recentLap4.Dispose ();
				recentLap4 = null;
			}

			if (widthOfPgrView != null) {
				widthOfPgrView.Dispose ();
				widthOfPgrView = null;
			}
		}
	}
}
